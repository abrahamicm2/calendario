<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/
Route::get('/', 'WellcomeControler@wellcome');

Auth::routes();

Route::get('/home', 'HomeController@index');

//Route::get('evento/{$id}/destroy', 'EventsController@destroy');


Route::resources([
    'event' => 'EventsController'
    
]);

//Route::get('/prueba', 'EventsController@prueba');


Route::get('/formulario3', 'WellcomeControler@formulario3');

Route::get('/formulario2', function(){
	return view('formulario2');
});

Route::get('/formulario', function(){
	return view('formulario');
});

Route::get('/see/{id}', 'EventsController@see');

Route::get('/users', 'UsersController@users');
Route::get('/users/{id}/edit/{type}', 'UsersController@edit');

Route::put('/users/{id}', 'UsersController@update')->name('admin.users.update');

Route::delete('/users/{id}', 'UsersController@destroy')->name('admin.users.destroy');

Route::get('/diary','EventsController@diary' )->name('diary');
Route::get('/notifications', 'EventsController@notifications');
Route::get('/prueba', 'EventsController@prueba');
Route::get('/notifications/{id}', 'EventsController@dnotifications');

Route::resources([
    'jobs' => 'JobsController'
    
]);

Route::post('/registers', [
	'as' => 'registers',
	'uses' =>'Auth\RegisterController@create'

]);

//Route::post('ajaxlogout', 'LoginController@ajaxlogout');






// Route::get('/diary', function () {
//     return view('diary');
// });

Route::get('sendemail', function () {

    $data = array(
        'name' => "Curso Laravel",
    );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('abrahamcodero@gmail.com', 'Curso Laravel');

        $message->to('abrahamicm2@gmail.com')->subject('test email Curso Laravel');

    });

    return "Tú email ha sido enviado correctamente";

});

//Social Login
Route::get('social/{provider?}', 'SocialController@getSocialAuth');
Route::get('social/callback/{provider?}', 'SocialController@getSocialAuthCallback');