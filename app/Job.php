<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    protected $table = "jobs";
     protected $filable = ['title','start','end','description','address','web','notified','confirmed','user','dstart','dend','category','pdf'];
}
