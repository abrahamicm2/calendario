<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    //
    // public static function boot(){
    // 	parent::boot();
   	// 	static::created(function($service_request){
    // 		event::fire('party_request.created', $party_request);
    // 	});
    // }

    protected $table = "parties";

    protected $fillable = ['id', 'name','description','start','end','address','confirmado','image','pdf','user','notified','dstart','dend','category','web'];
}


