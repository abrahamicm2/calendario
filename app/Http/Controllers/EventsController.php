<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
 
use Illuminate\Http\Request;

use App\Http\Requests\EventCreateRequest;

use App\Event;
use App\User;
use App\Party;
use Auth;
use Session;
use Redirect;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
       
    }
    public function index()
    {
        //
      // $parties = Party::all();
      // use App\Party;
      $parties= Party::all();
       return view ('create.dasboard', compact('parties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parties= Party::all();
        return view ('create.index', compact('parties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventCreateRequest $request)
    {
     
      if (Auth::user()->type=='member')
        {
         
         if (Party::where('user', '=', Auth::user()->name )->count() >= 1) {
            Session::flash("success", "only one event per organization is allowed");
           return redirect('/')->with('message','store');
         }

        }

        $parties = new Party(array(
            'name'=>$request->get('name'),
            'description'=>$request->get('description'),
            'category'=>$request->get('category'),
            'start'=>$request->get('start'),
            'end'=>$request->get('end'),
            'dstart'=>$request->get('dstart'),
            'dend'=>$request->get('dend'),
            'address'=>$request->get('address'),
            'web'=>$request->get('web'),
                ));

                $parties->user = Auth::user()->name;

                if (Auth::user()->type=='admin'){
                    $parties->confirmado= $request->get('confirmado');
                    $parties->notified = $request->get('notified');
                }

                if ($request->hasFile('image')){
                    $parties->image =  $request->file('image')->store('public');
                    }
            
                    if ($request->hasFile('pdf')){
                        $parties->pdf =  $request->file('pdf')->store('public');
                    }
         $parties->save();
        
           //
         Session::flash("success", "Event stored");
           return redirect('/')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // $parties = Party::find($id);
       // $parties->delete();
       //  return redirect('/event');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
      $parties= Party::find($id);
       
       return view('create.edit', compact( 'parties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventCreateRequest $request, $id)
    {
        //
        
       //return dd($request->all());
       $parties = Party::find($id);
       //return dd($parties->confirmado);
       //$parties->fill($request->all());
       //$parties->fill($request->all());
      $parties->name = $request->get('name');
      $parties->description = $request->get('description');
      $parties->category = $request->get('category');

      $parties->start = $request->get('start');
      $parties->end = $request->get('end');
      $parties->dstart = $request->get('dstart');
      $parties->dend = $request->get('dend');
      $parties->address = $request->get('address');
      $parties->web = $request->get('web');



     //$parties->user = Auth::user()->name;
       
                       if (Auth::user()->type=='admin'){
                         $parties->confirmado= $request->get('confirmado');
                         $parties->notified = $request->get('notified');
                       }
       
                       if ($request->hasFile('image')){
                         $parties->image =  $request->file('image')->store('public');
                           }
                   
                           if ($request->hasFile('pdf')){
                             $parties->pdf =  $request->file('pdf')->store('public');
                           }
         if (Auth::user()->type=='member'){
                          $jobs->confirmado= "no";
                        }

        
       $parties->save();
        //return$parties;

        //return redirect('event');
       Session::flash("success", "Event updated");
        return redirect('/')->with('message','store');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // $this->user->delete();
      // $message = $this->user->full_name . ' fue eliminado de nuestros registros '; 
      // if ($request->ajax()) {
      //   return response->json([
      //       'id' => $this->user->id,
      //       'message' => $message
      // ]);
      // } 


        $parties = Party::find($id);
        $parties->delete();
        Session::flash("success", "Event deleted");
        return redirect('/');
    }
    public function prueba()
    {
      
      return view("part.updateuser");
      return dd(Auth::user()->type=='admin');
      return dd(Party::all()->toJson());
      return dd(Party::find($id));
      return dd( Auth::user()->name);
      return dd(Party::where('user', '!=', Auth::user()->name)->count());

       // return dd($request->all());
      //return dd($request->get('category'));
      // if (Auth::user()->type=='member'){
         
      //    if (Party::where('user', '=', Auth::user()->name )->count() > 1) {
      //      return "se permite solo un evento por miembro";
      //    }

      //   return "soy un member";
      //   return dd(Party::all())
         
      // }


       return view('prueba');
    }

    public function see($id)
    {
        $parties= Party::find($id);
        
        return view('create.see', compact( 'parties'));
     }

     public function diary()
     {
         
     
      
       
        $users= User::all();
        $parties= Party::all();
        return view ('diary', compact(['users','parties']));
      
        
     }

      public function notifications()
     {
         $parties= Party::all();
       return view ('notifications', compact('parties'));
       
   
     }
        public function dnotifications($id)
     {
        $parties= Party::find($id);
        $parties->notified="si";
        $parties->save();

        return Redirect::to('/event/'. $id. '/edit');

        
       
   
     }
 
 
    
}
