<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Party;
use App\Job;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $jobs= Job::all();
         $users= User::all();
        $parties= Party::all();
       // return dd($jobs);
        return view ('index', compact(['users','parties','jobs']));
    }
}
