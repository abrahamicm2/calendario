<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\Party;
use Auth;
use Session;
use Redirect;
use App\Notification;
use Validator;
use Hash;


class UsersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'create', 'store', 'update', 'destroy'
        ]]);
        $this->middleware('admin', ['only'=>'users']);
    }

    public function users()
    {
        $users= User::all();
        $parties= Party::all();
        return view ('users', compact(['users','parties']));
       
    }
    public function destroy($id , Request $request)
    {

        
        $users = User::find($id);
        $users->delete();
        $message =  'el usuario ' .$users  .' fue eliminado de nuestros registros '; 

        if ($request->ajax()) {
          return response()->json([
            
            'message' => $message
            
          ]);
        }

        
       
        // return redirect('/');
    }
    //$party = Party::find($id);

    public function edit($id,$type)
    {
        $user = User::find($id);
        //return $id;
        $user->type = $type;
        
        $user->save();

        
        
        return redirect('/users');
    }

    public function update(Request $request, $id)
    {
        //

        
       //return dd(Auth::user()->type);
       //return dd( $request->get("type") );
       $user = User::find($id);

       // si el usuario autenticado es miemro el usuario actualizado es miembro
      if(Auth::user()->type=='member')
      {
        $user->type ="member";
      }
      // si el usuario autenticado es administrador el usuario  actualizado es administrador
      if(Auth::user()->type=='admin')
      {
        
        $user->type ="admin";

        if($request->get("type") != null)
        {
          $user->type = $request->get("type");
        }
      }
       //return dd($user);



      $user->name = $request->get('name');
      $user->lastname = $request->get('lastname');
      $user->email = $request->get('email');
      
      if ($request->get("password") != null) 
      {

        $rules = [
        'mypassword' => 'required',
        'password' => 'required|confirmed|min:6|max:18'
        ];
        
        $messages = [ 
        'mypassword.required' => ' the field is requiered ' , 
        'password.required' => ' the field is requiered ' , 
        'mypassword.required' => ' the fields do not match ' , 
        'password.min' => ' the minimum allowed are 6 characters ' , 
        'password.max' => ' the maximum allowed are 18 characters ' 
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails())
        {
        return redirect('/')->withErrors($validator);
        }
        else 
        {
          if (Hash::check($request->mypassword, Auth::user()->password))
          {
            
            
                $user->password = bcrypt($request->password);
                
            //return redirect('/')->with('status', 'Paword change successfull');
          }
          else 
          {
            Session::flash("warning", "Incorrect Credentials");
            return redirect('/')->with('message', ' incorrect credentials');
          }
        }
      }
        
      
      $user->save();
       Session::flash("success", "User updated");
        return redirect('/')->with('message','store');
    }

  public function updatePassword(Request $request) 
  {
   


  }

}
