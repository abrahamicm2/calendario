<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\JobRequest;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedF;
use App\Event;
use App\User;
use App\Party;
use App\Job;
use Auth;
use Session;
use Redirect;


class JobsController extends Controller
{
   
    //
    public function __construct()
    {
       

        
        $this->middleware('auth', ['only' => [
            'create', 'store', 'update', 'destroy'
        ]]);
       
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $jobs = Job::all();
        return view("jobs.index" , compact(["jobs","parties"]));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parties = Party::all(); 
        //return view('jobs.create', compact(["jobs","parties"]));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //  //return  dd($request->all());
        // //
        //  //return $request;
        // $parties = Party::all(); 
        if (Auth::user()->type=='member')
          {
           
           if (Job::where('user', '=', Auth::user()->name )->count() >= 1) {
              Session::flash("success", "only one job per organization is allowed");
             return redirect('/')->with('message','store');
           }

          }

         
         $jobs = new Job();

        //  //return  dd($request->get('image'));
             
             $jobs->title =$request->get('title');
             $jobs->pdf =$request->get('pdf');
             $jobs->description =$request->get('description');
             $jobs->start =$request->get('start');
             $jobs->end =$request->get('end');
             $jobs->dstart =$request->get('dstart');
             $jobs->dend =$request->get('dend');
             $jobs->address =$request->get('address');      
             $jobs->web =$request->get('web');
              $jobs->category =$request->get('category');
 $jobs->user = Auth::user()->name;

    
             if ($request->hasFile('image')){
                $jobs->image =  $request->file('image')->store('public');

             }
          


                 if (Auth::user()->type=='admin'){
                     $jobs->confirmed= $request->get('confirmed');
                     $jobs->notified = $request->get('notified');
                 }
                 

         $jobs->save();

          if ($request->ajax()) {
          return response()->json([
            
            'message' => "hola"
            
          ]);
      }
         
            Session::flash("success", "Job stored");
                  return redirect('/')->with('message','store');

           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $parties = Party::all(); 

         $jobs = Job::find($id);

                return view("jobs.index" , compact(["jobs","parties"]));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $parties = Party::all(); 
        $jobs= Job::find($id);




       
       return view('jobs.edit', compact(["jobs","parties"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {
        //
        //return dd($id);

         //return dd($request->all());
        $parties = Party::all(); 

    

        $parties = Party::all(); 

         $jobs = Job::find($id);
         //return dd($jobs);
             $jobs->title = $request->get('title');
             $jobs->pdf = $request->get('pdf');
             $jobs->description = $request->get('description');
             $jobs->start =$request->get('start');
             $jobs->end =$request->get('end');
             $jobs->address =$request->get('address');      
             $jobs->web =$request->get('web');
             $jobs->end =$request->get('end');
             $jobs->dstart =$request->get('dstart');
             $jobs->category =$request->get('category');
             //$jobs->notified = $request->get('notified');
               //$jobs->user = Auth::user()->name;
       
                       if (Auth::user()->type=='admin'){
                         $jobs->confirmed= $request->get('confirmed');
                         $jobs->notified = $request->get('notified');
                       }
                       if (Auth::user()->type=='member'){
                         $jobs->confirmed= "no";
                        
                       }
       
                     if ($request->hasFile('image')){
                         $jobs->image =  $request->file('image')->store('public');
                           }
                   
      
        if (Auth::user()->type=='member'){
                         $jobs->confirmed= "no";
                       }
        $jobs->save();

      Session::flash("success", "Job updated");
                 return redirect('/')->with('message','store');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobs = Job::find($id);
       $jobs->delete();
       Session::flash("success", "Job deleted");
        return redirect('/');
       
       return view('jobs.index', compact( 'jobs'));
    }
}
