<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    // $table->increments('id');
    // $table->string('name');
    // $table->string('description');
    // $table->string('day');
    // $table->string('start');
    // $table->string('end');
    // $table->string('address');
    // $table->string('image')->default('Null');
    // $table->string('pdf')->default('Null');;
    // $table->enum('confirmado',['si','no'])->default('no');
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required',
            'description' => 'required',
           
            'start' => 'required',
            'end' => 'required',
            'dstart' => 'required',
            'dend' => 'required',
            'address' => 'required',
            'image' => 'image',
            'pdf' =>'mimes:pdf'

        ];
    }
}
