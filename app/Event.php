<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $table = "events";

    protected $filable =['description','confirmado','created','sunrise','category_id','user_id','id','category','web'];


    public function categories(){
    	return $this->belongsToMany('App\Category')->withTimestamps();
    }
}

 
    