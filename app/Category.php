<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";

    protected $filable =[name];

public events(){
    	return $this->belongsToMany('App\Event')->withTimestamps();
    }

}
