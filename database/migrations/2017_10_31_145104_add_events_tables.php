<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->text('description');
            $table->enum('confirmado',['si','no'])->default('no');
            $table->date('created');
            $table->time('sunrise');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('category_event', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('category_id')->unsigned();
                    $table->integer('event_id')->unsigned();

                    $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
                    $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');

                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

