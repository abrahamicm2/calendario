<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotifiedParty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
   {
       //$table->string('lastname');
       Schema::table('parties', function (Blueprint $table) {
           $table->string('notified')->default("no");
       });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       //
       Schema::table('parties', function (Blueprint $table) {
           $table->string('notified');
       });
   }
}
