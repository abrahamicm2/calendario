@extends('app')
@section('content')
<div class="page-content">

@php
  $carbon = new \Carbon\Carbon();
  $date1 = $carbon->now();
  $date2 = new \Carbon\Carbon('tomorrow'); 
@endphp
@include('part.navadmin')
{{-- @include('part.navadmin2') --}}
@include('part.errorsany')
{{--  MODALS --}}
@include('part.eventstore2')
@include('part.jobstore2')
@include('part.modallogin')
@include('part.modalregister')
@include('part.updateevents')
@include('part.updatejobs')
@include('part.updateuser')
@include('part.header')
@include('part.carousel')
  @include('part.modalnotification')
  @include('part.sectionevents')
  @include('part.calendar')
  <div id="notyevents" style="margin-bottom: 30px"> </div>
  @include('part.editevents')
  @include('part.sectionclients')
  @include('part.sectionjobs')
  {{-- @include('part.notnotifications') --}}
{{--   @include('part.eventnotifications')
  @include('part.jobsnotifications') --}}
  @include('part.storejobs')
  <div id="notyjobs" style="margin-bottom: 30px"> </div>
 
  @include('part.editjobs')
  <div id="users1"> </div>
  @include('part.editusers')
  @include('part.contact')
</div> 
  <a href="#intro" class="go2top"><i class="fa fa-arrow-up"></i></a>
@endsection
 