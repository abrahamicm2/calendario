@extends('layouts.app')
<style type="text/css">
  form {
    display: inline;
  }
</style>
@section('content')




<br>

<div class="row">

  
<div class="col-md-12" style="padding: 60px">
<div class="row">
<div class="col-md-9">
    <h3>JOBS MANAGEMENT</h3>
    <a type="button" href="jobs/create" class="btn btn-success"> <span class="glyphicon glyphicon-plus "></span> New</a>                
</div>

<div class="col-md-3">
  <br> <br> <br>
    <div class="input-group" >
        <input type="text" class="form-control" placeholder="Search">
        <span class="input-group-addon"> <span class="glyphicon glyphicon-search"></span>
       
    </div>
</div>
 

</div>
<br>
  
<div class="table-responsive">
<table class="table">
  <thead>
     <tr>
        <th>Title</th>
        <th>Category</th>
        <!-- <th>Imagen</th> -->
        <th>Address</th>
        <th> Date Start</th>
        <th> Date End</th>
        <th>Description</th>
        <th>Status</th>
        <th>Action</th>
     </tr>
  </thead>
  <tbody>
     <tr>


     @foreach($jobs as $job)

     @if(Auth::user()->type=="member" && $job->confirmed=="no" && Auth::user()->name !=  $job->user)
     @else
    
      <td><a href="see/{{$job->id}}">{{ $job->title}}</a></td>
      <td>{{ $job->category}}</td>
      <!--  <td><img src="/storage/app/{{$job->image}}" class="img-responsive img-rounded"  style="width:204px;height:auto;"></td> -->
        <td>{{ $job->address}}</td>
        <td>{{$job->dstart}} -  {{$job->start}} </td>
        <td>{{$job->dend}} -  {{$job->end}} </td>
        <td>{{ $job->description }}</td>
       
       
        <td>@if($job->confirmed=="no") 
               

        <p><span class="glyphicon glyphicon-remove" style="color:red"></span></p>    
            @else
        <span class="glyphicon glyphicon-ok " style="color:#5cb85c"></span>
        
        </td>
        @endif
        
        <td> 
          <!-- aqui da royo poner ruta manual -->
        @if(Auth::user()->type=="admin")
           <a href="jobs/{{$job->id}}/edit"class="btn btn-success btn-sm">
           <span class="glyphicon glyphicon-edit"></span>
           </a>
           
          

           
      {!! Form::open(array('route' => ['jobs.destroy', $job->id ],'method' => 'delete')) !!}
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <a href="javascript: document.forms[1].submit()" class="btn btn-danger btn-sm">
           <span class="glyphicon glyphicon-trash"></span>
           </a>
      {!! Form::close() !!}
           @endif
           
          
           

        </td>
      
     </tr>
     @endif
     @endforeach
     
     
  </tbody>
</table>
</div>

</div>
  

</div>
          
<canvas id="the-canvas"></canvas>

@endsection

