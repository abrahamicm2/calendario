@extends('layouts.app')

@section('content')

<div class="container">
<div class="col-md-8">
  <h2 class="text-center">Edit</h2>
 
  @if(count($errors) > 0)
  <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" 
data-toggle="modal" data-target="#myModal"
id="ocultar"
style="display:none"
>Open Modal

</button>
<script>
$(document).ready(function(){
  $("#ocultar").click()
   
});

</script>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Required</h4>
      </div>
      <div class="modal-body">
      @foreach($errors->all() as $error)
{{$error}} <br>
@endforeach

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endif




  {!! Form::open(array('route' => ['jobs.update',$jobs->id],'method' => 'put','class'=>'form-horizontal')) !!}

  
   <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">

       
    <label for="title">Job title:</label>
      <input type="text" class="form-control" id="title" 
      placeholder="Here is a some text input" name="title" value="{{$jobs->title}}">
      
      <label >Times:</label><br>
      <input type="date" class="form-control" id="dstart" placeholder=" Date Start:" name="dstart" style="display: inline; width: 40%" value="{{$jobs->dstart}}"> 
      <input type="time" class="form-control" id="start" placeholder="Start:" name="start" style="display: inline; width: 40%" value="{{$jobs->start}}"> <br>
      <input type="date" class="form-control" id="dend" placeholder="Date:" name="dend" style="display: inline; width: 40%" value="{{$jobs->dend}}" >
      <input type="time" class="form-control" id="end" placeholder="End:" name="end" style="display: inline; width: 40%" value="{{$jobs->end}}">
<br>
       <label for="description">Descripcion</label>
      <input type="text" class="form-control" id="description" placeholder="Here is a some text input" name="description" value="{{$jobs->description}}">
      <label >Category:</label>
<select class="form-control" name="category">
  <option value="category 1">category 1</option>
  <option value="category 2">category 2</option>
  <option value="category 3">category 3</option>
  <option value="category 4">category 4</option>
</select><br>
        
        <label for="image" >image: <img src="/storage/app/{{$jobs->image}}" style="width: 300px"> </label>
        <input type="file" class="form-control" id="image" placeholder="Here is a some text input" 
        name="image" value="{{$jobs->image}}" style="display: none">
      </div>
    

    
     <br>
     <label for="address">address:</label>
      <input type="text" class="form-control" id="address" placeholder="Here is a some text input" name="address" value="{{$jobs->address}}">
      <br>
      <label for="web">Apply here:</label>
      <input type="text" class="form-control" id="web" placeholder="website Link" name="web" value="{{$jobs->web}}">
   
      	
      	<br>
  
      
  
     
       
    
    
    @if(Auth::user()->type=="admin")
   
    <label for="sel1">Confirmed:</label>
      <select class="form-control" id="sel1" name="confirmed">
      <option value="no">No</option>
    <option value="yes">Yes</option>
      </select>
      <br>


    <br>
    <input type="hidden" name="notified" value="yes" >
    @endif
    <button type="submit" class="btn btn-primary">Update</button>
    <a href="{{url('/jobs')}}" class="btn btn-danger">Cancel</a>
    {!! Form::close() !!}
  </div>
</div>


<!-- $errors->default->first("name") -->

@endsection