<div>
  <button id="header-log" class="uk-button uk-button-link white-color white-hover uk-open" aria-expanded="true">
  Log In
  </button>
  <div id="drop-login" uk-dropdown="mode: click; pos: bottom-center; delay-hide: 0;" class="uk-width-medium uk-dropdown uk-open uk-dropdown-bottom-center" style="top: 35.2px; left: 956.875px;">
    <div class="uk-child-width-1-1">
      <h4>Log In</h4>
      <form>
        <div class="uk-margin">
          <input name="email" placeholder="Email" class="uk-input" aria-required="true" aria-invalid="false" type="text">
        </div>
        <div class="uk-margin">
          <input name="password" placeholder="Password" class="uk-input" aria-required="true" aria-invalid="false" type="password">
        </div>
        <div class="uk-margin">
          <button type="submit" class="uk-button uk-width-1-1 uk-button-primary">
          <i class="fa fa-sign-in"></i>Log In
          </button>
        </div>
        <div class="uk-margin">
          Do not have an account? 
          <a>Start by performing a search.</a>Or 
          <a href="/password" class="">Recovery password.</a>	
        </div>
      </form>
      <hr>
      <div class="uk-button uk-width-1-1 btn-facebook is_disabled">
        <i class="fa fa-facebook"></i>
        Log In with Facebook
      </div>
      <br>
      <br>
      <button id="google_btn_float" class="uk-button uk-width-1-1 btn-google is_disabled">
      <i class="fa fa-google-plus"></i>Log In with Google
      </button>
    </div>
  </div>
</div>
