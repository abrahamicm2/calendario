<!DOCTYPE html>
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8"/>
  <title>CITY COONECTIONS INC |GET CONNECTE </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta content="" name="description"/>
  <meta content="" name="author"/>
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.css" rel="stylesheet" type="text/css" />
  <link href='http://fonts.googleapis.com/css?family=Hind:400,500,300,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
  <link href="public/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link href="public/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
  <link href="public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <!-- UIkit CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/css/uikit.min.css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
  <link href="public/assets/pages/css/animate.css" rel="stylesheet">
  <link href="public/assets/plugins/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <link href="public/assets/plugins/cubeportfolio/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet">
  <!-- END PAGE LEVEL PLUGIN STYLES -->
  <!-- BEGIN THEME STYLES -->
  <link href="public/assets/onepage2/css/custom.css" rel="stylesheet" type="text/css"/>
  <link href="public/assets/onepage2/css/layout.css" rel="stylesheet" type="text/css"/>
  <!-- END THEME STYLES -->
  <link rel="shortcut icon" href="favicon.ico"/>
  <!-- BEGIN CALENDAR STYLES -->
  <!-- END CALENDAR STYLES -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.js'></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script type="text/javascript"></script>
  <style>
    .header {
    /*background: blue;*/
    color: white;
    height: 50px;
    }
    .span {
    padding-top: 15px;
    }
    .izquierda { 
    background-color: #424a5d;
    color:white;
    height: 100vh;
    }
    @media (max-width: 992px) {   
    .izquierda {display: none;
    }
    }
    
    .c-black{
    margin-right: 10px;
    font-size: 28px;
    color: #e0e0e0;
    cursor: pointer;
    }
    .c-black:hover {
    color: #cccccc;
    }
    .green {background-color: green;}
    .yellow {background-color: yellow;}
    .blue {background-color: blue;}
    .red {background-color: red;}
    .black {background-color: black;}
    .block {display: block;}
    .inline {display: inline;}
    .inline-block {display: inline-block;}
    .none {display: none;}
    .absolute {position: absolute;}
    .fixed {position: fixed;}
    .relative {position: relative;}
    .l {float: left;}
    .r {float: right;}
    .w-0 { with:0%; }
    .w-5 { with:5%; }
    .w-10 { with:10%; }
    .w-15 { with:15%; }
    .w-20 { with:20%; }
    .w-25 { with:25%; }
    .w-30 { with:30%; }
    .w-35 { with:35%; }
    .w-40 { with:40%; }
    .w-45 { with:45%; }
    .w-50 { with:50%; }
    .w-55 { with:55%; }
    .w-60 { with:60%; }
    .w-65 { with:65%; }
    .w-70 { with:70%; }
    .w-75 { with:75%; }
    .w-80 { with:80%; }
    .w-85 { with:85%; }
    .w-90 { with:90%; }
    .w-95 { with:95%; }
    .w-100 { with:100%; }
    .h-120 { height: 120px; }
    .category {
    margin-top: 0px;
    color: #17bf42;
    display: inline-block;
    line-height: 1.3;
    font-weight: bold;
    font-size: 18px;
    word-break: break-word;
    width: 80%;
    font-family: 'Source Sans Pro', sans-serif !important;
    font-weight: 300 !important;
    font-style: normal !important;
    text-decoration: none;
    }
    .color-black {color:black;}
    .category_1 {color: #bf1756;} /*rojo*/
    .category_2 { color: #1776bf; } /*azul*/
    .category_3 {color: #bfbd17;} /*amarillo*/
    .category_4 {color: #bf8a17; } /*anaranjado*/
    .category_5 {color: #17bf42; } /*verde*/
    .category_1b {background-color: #bf1756;} /*rojo*/
    .category_2b {background-color: #1776bf; } /*azul*/
    .category_3b {background-color: #bfbd17;} /*amarillo*/
    .category_4b {background-color: #bf8a17; } /*anaranjado*/
    .category_5b {background-color: #17bf42; } /*verde*/
  
    .card-1 {
    background: #f9f9f9;
    border: 1px solid #e9e9e9;
    /* margin-top: 112px; */
    padding: 8px;
    margin: 0;
    margin-bottom: 8px;
    
    width: 270px;
    }
    .num , .num2{
    background: green;
    width: 40px;
    display: block;
    height: 50px;
    padding-top: 9px;
    }
    .num2 {
    background: #17bf42;
    }
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 0;
    background-color: none;
    }
    .flex-center {
    display: flex;
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    justify-content: space-between;
    }
    .flex-center img {
    margin: 0 auto;
    }
    .item-start {
    align-self: flex-start;
    height: 400px;
    }
    .item-start img {
    max-height: 150px;
    margin: 0 auto;
    }
    .month {
    color: #fff;
    height: 8px;
    text-align: center;
    font-size: 4pt;
    text-transform: uppercase;
    }
    .day {
    text-shadow: 0 -1px 1px rgba(0,0,0,0.2);
    color: #fff;
    margin-top: -2px;
    text-align: center;
    font-size: 13pt;
    margin: -3px 0;
    text-transform: uppercase;
    }
    .week {
    color: #fff;
    margin-top: -3.5px;
    text-align: center;
    font-size: 4pt;
    padding: 1px;
    letter-spacing: 1px;
    text-transform: uppercase;
    }
    .contnum{
    width: 18%;
    display: inline-block;
    }
    .form-wrap {
    height: auto;
}
.form-control {
    min-height: 30px; 
}
.btn-danger.btn-md {
    padding: 8px;
    font-size: 15px;
    }
    .service-bg .services .services-wrap:before {
     border: none;
    margin-bottom: 10px;
    padding-top: 100px;
    }
    .clients-quotes {

    padding-bottom: 30px !important;
}
.service-bg {
    padding-top: 50px !important;
   
}
  .services-wrap {
     
    margin-bottom: 30px !important;
    }
    .form-control {
      display: block;
      width: 100%;
      height: 20px;
      padding: 0px;
      font-size: 14px;
      line-height: 1.42857143;
      color: #555;
      background-color: #fff;
      background-image: none;
      border: 1px solid #ccc;
      border-radius: 0px;
    }
  </style>
</head>
<body class="page-header-fixed">
@yield('content')

  <!-- BEGIN CORE PLUGINS -->
  <!--[if lt IE 9]>
  <script src="public/assets/plugins/respond.min.js"></script>
  <script src="public/assets/plugins/excanvas.min.js"></script>
  <![endif]-->
  <script src="public/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->


  <script src="public/assets/plugins/jquery.easing.js" type="text/javascript"></script>
  <script src="public/assets/plugins/jquery.parallax.js" type="text/javascript"></script>
  <script src="public/assets/plugins/smooth-scroll/smooth-scroll.js" type="text/javascript"></script>
  <script src="public/assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script>
  <!-- BEGIN CUBEPORTFOLIO -->
  <script src="public/assets/plugins/cubeportfolio/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
  <script src="public/assets/onepage2/scripts/portfolio.js" type="text/javascript"></script>
  <!-- END CUBEPORTFOLIO -->
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="public/assets/onepage2/scripts/layout.js" type="text/javascript"></script>
  <script src="public/assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>
  <script src="public/assets/onepage2/scripts/custom.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- UIkit JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/js/uikit.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.35/js/uikit-icons.min.js"></script>
  <script>
    jQuery(document).ready(function() {
        Layout.init();
    });
  </script>
  <!-- script calendar -->
  <script>
    $(document).ready(function() {
    
    
        
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: 
            [
    
                @foreach($parties as $party)
                
                @if($party->confirmado=="si")
{
    
                  @if($party->dstart == $party->dend) 
                  title: '{{$party->name}}',
                  @else
                  
                    title: '{{$party->name}} {{$party->start}}',
                  @endif
    
    
                      @switch($party->category)
                        @case('category_1')
                            color: 'red',
                          @break

                        @case('category_2')
                           color: 'rgb(23, 191, 66)', 
                          @break
    
    
                        @case('category_3')
                            
                           color: 'blue', 
                          @break

                          @case('category_4')
                              
                             color: 'yellow', 
                            @break

                            @case('category_5')
                                
                               color: 'orange', 
                              @break
                    @endswitch
                    @if($party->dstart == $party->dend) 
                     start: '{{$party->dstart}}T{{$party->start}}'
                    @else
                     start: '{{$party->dstart}}',    //T{{--$party->start--}}',
                     end: '{{$party->dend}}' 
                    @endif
                   
                    
    
                     
                },
    
                @endif
                @endforeach
    
                 @foreach($jobs as $job)
                // if($party->confirmado=="si")
                @if($job->confirmed=="yes")
                {
                    title: '{{$job->name}}',
                    start: '{{$job->dstart}}T{{$job->start}}',
                    end: '{{$job->dend}}T{{$job->end}}'
                },
    
                @endif
                @endforeach
    
                
    
            ]
        });
        
    
    
    });
    
    
    
  </script>
  <script>
    $(document).ready(function(){   
      $('<a  class="c-black" id="bt-scuared" style="margin-right: 4px;"><span class="glyphicon glyphicon-th-large"></span></a><a  class="c-black" id="bt-list" style="margin-right: 4px;"><span class="glyphicon glyphicon-th-list"> </span></a>').insertBefore(".fc-month-button");
    
    $(".fc-view-container").hide();
    $("#scuared").show();
      $("#list").hide();
    $('.fc-button').removeClass('fc-state-active');
      $("#bt-list").click(function(){
        $("#scuared").hide();
        $(".fc-view-container").hide();
        $("#list").show()
        $('.fc-button').removeClass('fc-state-active');
      });
      $("#bt-scuared").click(function(){
        $("#list").hide();
        $(".fc-view-container").hide();
        $("#scuared").show()
        $('.fc-button').removeClass('fc-state-active');
      });
      $(".fc-button ").click(function(){
        $("#list").hide();
        $(".fc-view-container").show();
        $("#scuared").hide()
      });
    
    
    
    
    
    
    $(".fc-listWeek-button").click(function(){
      
        $(".fc-listWeek-view").hide();
        
        $(".fc-view").show()
        $(".fc-agendaWeek-view").show()
        $("fc-agenda-view").show()
      });
    
    
    //
    });
      
  </script>
  <script>
 
</script> 

</body>
<!-- END BODY -->
</html>