<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Calendar</title>
    
    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.css' rel='stylesheet' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.print.css' rel='stylesheet' media='print' />
    


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    
    <style>
         .header {
         /*background: blue;*/
         color: white;
         height: 50px;
         }
         .span {
         padding-top: 15px;
         }
         .izquierda { 
         background-color: #424a5d;
         color:white;
         height: 100vh;
         }
         @media (max-width: 992px) {   
         .izquierda {display: none;
         }
         }
         #calendar {
            max-width: 900px;
            margin: 0 auto;
         }
         .form-control2 {
             color: #97a7b4;
             font-size: 17px;
             font-weight: 300;
             min-width: 100px;
             min-height: 50px;
             border-radius: 0;
             box-shadow: none;
             border-color: #3b4e5f;
             background: transparent;
         }
      </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                  <a class="navbar-brand" href="{{url('diary')}}">
                    <span class="glyphicon glyphicon-calendar"></span> 
               </span>
                    </a>
                    <a href="{{url('event')}}" class="navbar-brand" >
                    <span class="glyphicon glyphicon-home"></span>
                    </a>
                    @guest
                    @else
                    <a href="{{url('notifications')}}" class="navbar-brand">
                        <span>
                         Notifications   
                        </span>
                        <span class="badge">
                         @isset($parties)
                          {{$parties->where('notified', "no")->count()}}
                          @endisset
                             
                        </span>

                           <a class="navbar-brand" href="{{url('jobs')}}">
                            
                            Jobs
                             </a>
                        

                    </a>
                    @endguest
                        
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="/{{ route('login') }}">Login</a></li>
                            <li><a href="/{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="/{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    @yield('script')
</body>
</html>
