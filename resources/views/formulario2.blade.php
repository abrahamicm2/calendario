<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://unpkg.com/vue"></script>
  <style type="text/css">
    .inline-block {
      display: inline-block;
    }
    .mw100 {
      max-width: 100px;
    }
    .mw250 {
      max-width: 250px;
    }
    .flex {
      display: flex;
      flex-wrap: wrap;
    }
    .w250 {
      width: 250px;
    }
  </style>
  </head>

  <body>
    <br>  <br><br>
    
<div class="container" >
  <form action="" class="form-horizontal">
    <div>

      <label> Name</label> 
      <input type="text" name="name" id="" class="form-control input-sm col-sm-10">
    </div>
<br>
<div>
<div id="app">
    <div class="inline-block" >
      <label> START</label> <br>
      <input  type="date" name="" id="" class="form-control input-sm input-sm" style="max-width: 124px; display: inline-block;"> 
      <div style="max-width: 124px; display: inline-block;" v-show="!allDayShow" >
        <select name="" id="" class="form-control input-sm input-sm"  >
          <option value='08:00'>8:00 Am</option>
          <option value='08:30'>8:30 Am</option>
          <option value='09:00'>9:00 Am</option>
          <option value='09:30'>9:30 Am</option>
          <option value='10:00'>10:00 Am</option>
          <option value='10:30'>10:30 Am</option>
          <option value='11:00'>11:00 Am</option>
          <option value='11:30'>11:30 Am</option>
          <option value='12:00'>12:00 m</option>
          <option value='12:30'>12:30 Am</option>
          <option value='13:00'>1:00 Pm</option>
          <option value='13:30'>1:30 Pm</option>
          <option value='14:00'>2:00 Pm</option>
          <option value='14:30'>2:30 Pm</option>
          <option value='15:00'>3:00 Pm</option>
          <option value='15:30'>3:30 Pm</option>
          <option value='16:00'>4:00 Pm</option>
          <option value='16:30'>4:30 Pm</option>
          <option value='17:00'>5:00 Pm</option>
          <option value='17:30'>5:30 Pm</option>
          <option value='18:00'>6:00 Pm</option>
          <option value='18:30'>6:30 Pm</option>
          <option value='19:00'>7:00 Pm</option>
          <option value='19:30'>7:30 Pm</option>
          <option value='20:00'>8:00 Pm</option>
          <option value='20:30'>8:30 Pm</option>
          <option value='21:00'>9:00 Pm</option>
          <option value='21:30'>9:30 Pm</option>
          <option value='22:00'>10:00 Pm</option>
          <option value='22:30'>10:30 Pm</option>
          <option value='23:00'>11:00 Pm</option>
          <option value='23:30'>11:30 Pm</option>
          <option value='00:00'>12:00 Am</option>
          <option value='00:30'>12:30 Am</option>
          <option value='01:00'>1:00 Am</option>
          <option value='01:30'>1:30 Am</option>
          <option value='02:00'>2:00 Am</option>
          <option value='02:30'>2:30 Am</option>
          <option value='03:00'>3:00 Am</option>
          <option value='03:30'>3:30 Am</option>
          <option value='04:00'>4:00 Am</option>
          <option value='04:30'>4:30 Am</option>
          <option value='05:00'>5:00 Am</option>
          <option value='05:30'>5:30 Am</option>
          <option value='06:00'>6:00 Am</option>
          <option value='06:30'>6:30 Am</option>
          <option value='07:00'>7:00 Am</option>
          <option value='07:30'>7:30 Am</option>
        </select>
       </div>
    </div>
    <div class="inline-block" v-show="!allDayShow">
      <label> END</label><br>
      <input  type="date" name="" id="" class="form-control input-sm input-sm" style="max-width: 124px; display: inline-block;" v-bind:disabled="isButtonDisabled" >
    </div>
    <div class="inline-block"  >
      <input type="checkbox" name="vehicle" value="Bike"   v-model="isButtonDisabled" v-show="!allDayShow" > 
      <span v-show="!allDayShow">no end date</span> 
      <br>
      <div style="max-width: 124px; display: inline-block;"v-show="!allDayShow">
        <select name="" id="" class="form-control input-sm input-sm" v-bind:disabled="isButtonDisabled" >
          <option value='12:00'>12:00 m</option>
          <option value='12:30'>12:30 Am</option>
          <option value='13:00'>1:00 Pm</option>
          <option value='13:30'>1:30 Pm</option>
          <option value='14:00'>2:00 Pm</option>
          <option value='14:30'>2:30 Pm</option>
          <option value='15:00'>3:00 Pm</option>
          <option value='15:30'>3:30 Pm</option>
          <option value='16:00'>4:00 Pm</option>
          <option value='16:30'>4:30 Pm</option>
          <option value='17:00'>5:00 Pm</option>
          <option value='17:30'>5:30 Pm</option>
          <option value='18:00'>6:00 Pm</option>
          <option value='18:30'>6:30 Pm</option>
          <option value='19:00'>7:00 Pm</option>
          <option value='19:30'>7:30 Pm</option>
          <option value='20:00'>8:00 Pm</option>
          <option value='20:30'>8:30 Pm</option>
          <option value='21:00'>9:00 Pm</option>
          <option value='21:30'>9:30 Pm</option>
          <option value='22:00'>10:00 Pm</option>
          <option value='22:30'>10:30 Pm</option>
          <option value='23:00'>11:00 Pm</option>
          <option value='23:30'>11:30 Pm</option>
          <option value='00:00'>12:00 Am</option>
          <option value='00:30'>12:30 Am</option>
          <option value='01:00'>1:00 Am</option>
          <option value='01:30'>1:30 Am</option>
          <option value='02:00'>2:00 Am</option>
          <option value='02:30'>2:30 Am</option>
          <option value='03:00'>3:00 Am</option>
          <option value='03:30'>3:30 Am</option>
          <option value='04:00'>4:00 Am</option>
          <option value='04:30'>4:30 Am</option>
          <option value='05:00'>5:00 Am</option>
          <option value='05:30'>5:30 Am</option>
          <option value='06:00'>6:00 Am</option>
          <option value='06:30'>6:30 Am</option>
          <option value='07:00'>7:00 Am</option>
          <option value='07:30'>7:30 Am</option>
          <option value='08:00'>8:00 Am</option>
          <option value='08:30'>8:30 Am</option>
          <option value='09:00'>9:00 Am</option>
          <option value='09:30'>9:30 Am</option>
          <option value='10:00'>10:00 Am</option>
          <option value='10:30'>10:30 Am</option>
          <option value='11:00'>11:00 Am</option>
          <option value='11:30'>11:30 Am</option>
        </select>
      </div>
        <input type="checkbox" name="" value="" v-model="allDayShow"  > All-day
    </div>
 </div>               

    
    <div class="inline-block">
      <label> CATEGORIES</label> 
       <select class="form-control input-sm w250">
         <option value="option1">option1</option>
         <option value="option2">option2</option>
         <option value="option3">option3</option>
         <option value="option4">option4</option>
      </select> 

    </div>
    <div class="inline-block">
      <label> TAGS</label> 
       <select class="form-control input-sm w250">
         <option value="option1">option1</option>
         <option value="option2">option2</option>
         <option value="option3">option3</option>
         <option value="option4">option4</option>
      </select> 
    </div>
    <div class="form-gruop">
      <label> Web</label>
<input type="" name="" placeholder="Web" class="form-control input-sm">
    </div>
       <div class="form-gruop">
      <label> Address</label>
<input type="" name="" placeholder="Web" class="form-control input-sm">
    </div>
    
<div class="form-gruop">
   <label> DESCRIPTION</label><br>
    
   <textarea class="form-control input-sm col-sm-12" rows="5">
   </textarea>    
 </div>

  <div class="form-gruop" >
    <div class="col-md-6" style="height: 400px">
      <label> Img</label>
      <div id="upload" >
        <input type="file" name="" id="image" > 
      </div>
   </div>
   <div class="col-md-6">
     <label>Pdf</label><br>
     <input type="file" name="" id="" class="">
   </div>
 </div>
 
</div>
<div class="form-gruop">
  <div class="col-md-12">
<button type="submit" class="btn btn-primary">Submit</button>
    
  </div>
</div>
  

  </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script type="text/javascript">
 
  function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#upload + img').remove();
            $('#upload').after('<img    style="height:250px"class="img-responsive" src="'+e.target.result+'"/>');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function () {
    
    filePreview(this);
});



</script>
<script type="text/javascript">
  var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      isButtonDisabled: false,
      allDayShow: false
    }
  })
</script>

  </body>
</html>