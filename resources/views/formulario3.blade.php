@auth
  <div class="container" id="events_store">
    <dir class="events-store">
      <span class="close" id="span_event_create">×</span>
      {!! Form::open(array('url' => 'event','method' => 'post','files' => true,'class'=>'form-horizontal')) !!}
        {{ csrf_field() }}
        <div class="form-group form">
          <input type="text" class="form-control" id="name" placeholder="Name" name="name"required>
          <input type="text" class="form-control" id="description" placeholder="Description" name="description"required>         
          <input type="date" class="form-control" id="dstart"  name="dstart" style="display: inline; width: 49%" required
          value="{{$date1->format('Y-m-d')}}"> 
          <input type="time" class="form-control" id="start" name="start" style="display: inline; width: 49%" value="08:00">
          <input type="date" class="form-control" id="dend"  name="dend" style="display: inline; width: 49%" required
          value="{{$date2->format('Y-m-d')}}">
          <input type="time" class="form-control" id="end" name="end" style="display: inline; width: 49%" value="12:00">
          <select class="form-control" name="category" required>
            <option >select category</option>
            <option value="category_1">category 1</option>
            <option value="category_2">category 2</option>
            <option value="category_3">category 3</option>
            <option value="category_4">category 4</option>
          </select>
          <input type="text" name="web" placeholder="Web Here"  class="form-control">
          <input type="text" class="form-control" id="address" placeholder="Address Here" name="address" required>
          <label for="image" class="form-control">image:</label>
          <input type="file" class="form-control" id="image" name="image"  required  > 
          <label for="pdf" class="form-control" >pdf:</label>
          <input type="file" class="form-control" id="pdf" style="display: none;" >
  @if(Auth::user()->type=="admin") 
          <input type="hidden" name="confirmado" value="no" >
          <input type="hidden" name="notified" value="si" >
  @endif
          <button type="submit" class="btn btn-danger" style="
            color: #fff;
            background-color: #337ab7;
            border: 2px solid #337ab7 !important;
            ">Create</button>
          <a  class="btn btn-danger" id="eventscancel">Cancel</a>
      {!! Form::close() !!}
    </dir>
  </div>
@endauth