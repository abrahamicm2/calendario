@extends('layouts.app')
@section('content')
<br>
<div class="row">
 
   <div class="col-md-12" style="padding: 60px">
      <div class="row">
         <div class="col-md-9">
            <h3>Usuarios</h3>
         </div>
         <div class="col-md-3">
            <br> <br> <br>
            <div class="input-group" >
               <input type="text" class="form-control" placeholder="Search">
               <span class="input-group-addon"> <span class="glyphicon glyphicon-search"></span>
            </div>
         </div>
      </div>
      <br>
      <div class="table-responsive">
         <table class="table">
            <thead>
               <tr>
                  <th>Name</th>
                  <th>Lastname</th>
                  <th>Email</th>
                  <th>Type</th>
                  <th>  
                     Action               
                  </th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  @foreach ($users as $user)                              
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->lastname }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                  
                  @if($user->email == "abrahamicm2@gmail.com"  || $user->email == "calendar@rock7.media" )
                  <b>Superadmin</b>
                  @else
                  <a id="re" style="display:none">{{$user->id}}</a>
                  <select name='type' id="type" onchange="myFunction()">
                  @if($user->type=="admin")
                  <option value="admin">admin</option>
                  <option value="member">member</option>
                  @else
                  <option value="member">member</option>
                  <option value="admin">admin</option>
                  @endif

                  @endif
                  
                  
                </select>
                  
                  </td>
                  <td>
      @if($user->email == "abrahamicm2@gmail.com" || $user->email =="calendar@rock7.media")


                  @else
                     <a href="users/{{ $user->id}}/delete" class="btn btn-danger btn-sm" href="">
                     <span class="glyphicon glyphicon-trash"></span>
                     </a>
                    @endif
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
<script>
function myFunction() {
    var x = document.getElementById("type").value;
    var a = document.getElementById("re");
    a.href="users/"+a.innerHTML+"/edit/"+x;
    //alert(a);
   a.click();
    
}
 

</script>

@endsection