@auth 
  <div class="dasboard">
    <ul>
      <li>
        <a  href="#1" class="glyphicon glyphicon-bell" data-toggle="modal" data-target="#modalnotifications" data-backdrop="false">
  @if($parties->where('notified', "no")->count() + $jobs->where('notified', "no")->count() != 0 && Auth::user()->type=="admin" )
          
          <span style="color: #ff0909;font-weight: 600;" class="spanhover">  
            {{$parties->where('notified', "no")->count() + $jobs->where('notified', "no")->count()}}
          </span>
            
  @endif
        </a>
      </li>
     <li id="licalendar">
        <a class="glyphicon glyphicon-calendar"  ></a>
        <ul class="ulcalendar">
          <li style="margin-bottom: -1px;"><a href=" " data-target="#myModal" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Event</a></li>
          <li style="margin-bottom: -1px;"><a href=" " style="width: 125px">-See Event</a> </li>
        </ul>
     </li>

  
     <li id="lijob" > 
        <a class="glyphicon glyphicon-list-alt" ></a>
        <ul class="uljob">
          <li style="margin-bottom: -1px;"><a href=" " data-target="#storejobsmodalform" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Job</a></li>
          <li style="margin-bottom: -1px;"><a href="http://eventr7m.ideveloper.technology/server.php#notyjobs" style="width: 125px">-See Job</a> </li>
        </ul>
      </li>
      
      <li id="liuser">
        <a  class="glyphicon glyphicon-user"></a>
        <ul class="uluser">  
          <li><a href="">-Edit User</a></li>
          <li><a href="http://eventr7m.ideveloper.technology/server.php#users1">-See User</a></li>
        </ul>
      </li>
    </ul>
  </div>


@endauth
{{-- <div class="dropdown">
  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
    <li><a href="#">HTML</a></li>
    <li><a href="#">CSS</a></li>
    <li><a href="#">JavaScript</a></li>
  </ul>
</div> --}} 