

@auth
{{-- si el usuario es tipo miembro se muestra el edit de usuarios --}}
@if(Auth::user()->type=="member")

@if (Job::where('user', '=', Auth::user()->name )->count() >= 1) 
 
  <div class="row">
    <!-- job index edit -->
    <div class="col-md-12" style="padding: 60px">
      <div class="row">
        <div class="col-md-9">
          <h3> My JOBS MANAGEMENT</h3>
          <a   class="btn btn-success" id="newjob"  data-toggle="modal" data-target="#storejobsmodalform" data-backdrop="false"> <span class="glyphicon glyphicon-plus "></span> New</a>                
        </div>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Image</th>
              <th>Category</th>
              <th>Description</th>
              <th>Web</th>
              <th>Address</th>
              <th> Date Start</th>
              <th> Date End</th>
              <th>Confirmed</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              @foreach($jobs as $job)
              @if(Auth::user()->type=="member" && $job->confirmed=="no" && Auth::user()->name !=  $job->user)
              @else
              @if(Auth::user()->type=="member")
             
              <input type="hidden" name="pdf" value="null">
              <td><input type="text" name="title" value="{{ $job->title}}"> </td>
              <td><img src="http://eventr7m.ideveloper.technology/storage/app/{{$job->image}}" class="img-responsive img-rounded"  style="width: 100px;height: 100px"></td>
              <td>
              
 @switch($job->category)
     @case("category_1")
         Arts & Culture
       @break
     @case("category_2")
         Edcation & Workshop
       @break
     @case("category_3")
         Music
       @break
     @case("category_4")
         Sport Fitness
       @break
     @case("category_5")
         Comunity
       @break                  
 @endswitch

              </td>
              <td>{{ $job->description }}</td>
              
              <td> {{$job->web}}</td>
              <td> {{ $job->address}}</td>
              <td>{{$job->dstart}} -- {{$job->start}}</td>
              <td>{{$job->dend}}--{{$job->end}} </td>
              
              <td>
                
                {{$job->confirmed}}
              </td>
             
              <td>
                <button  class="btn btn-success btn-sm" data-toggle="modal" data-target="#updatejobsmodalform{{$job->id}}" data-backdrop="false">
                <span class="glyphicon glyphicon-edit"></span>
                </button>
               
                {!! Form::open(array('route' => ['jobs.destroy', $job->id ],'method' => 'delete','class' => 'inline-form')) !!}
                {{ csrf_field() }}
                <button type="submit"  class="btn btn-warning btn-sm">
                <span class="glyphicon glyphicon-trash"></span>
                </button>
                {!! Form::close() !!}
                @endif      
              </td>
            </tr>
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endif


@endif
@endauth

