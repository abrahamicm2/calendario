
    <!-- BEGIN CLIENTS SECTION -->
    <section id="clients">
      <div class="clients">
        <div class="clients-bg">
          <div class="container">
            <div class="heading-blue">
              <h2>City Connections wants  to help you Get Connected.</h2>
              <p> Our weekly email provides events and opportunities around town. To submit your event or posting email us at info@cityconnectionsinc.org</p>
            </div>
          </div>
        </div>

        
        <!-- Clients Quotes -->
        <div class="clients-quotes">
          <div class="container">
            <div class="client-quote" id="client-quote-1">
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit euismod tincidunt ut laoreet dolore magna aliquam dolor sit amet consectetuer elit</p>
              <h4>Mark Nilson</h4>
              <span>Director</span>
            </div>
            <div class="client-quote" id="client-quote-2">
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit euismod tincidunt aliquam dolor sit amet consectetuer elit</p>
              <h4>Lisa Wong</h4>
              <span>Artist</span>
            </div>
            <div class="client-quote" id="client-quote-3">
              <p>Lorem ipsum dolor sit amet consectetuer elit euismod tincidunt aliquam dolor sit amet elit</p>
              <h4>Nick Dalton</h4>
              <span>Developer</span>
            </div>
            <div class="client-quote" id="client-quote-4">
              <p>Fusce mattis vestibulum felis, vel semper mi interdum quis. Vestibulum ligula turpis, aliquam a molestie quis, gravida eu libero.</p>
              <h4>Alex Janmaat</h4>
              <span>Co-Founder</span>
            </div>
            <div class="client-quote" id="client-quote-5">
              <p>Vestibulum sodales imperdiet euismod.</p>
              <h4>Jeffrey Veen</h4>
              <span>Designer</span>
            </div>
            <div class="client-quote" id="client-quote-6">
              <p>Praesent sed sollicitudin mauris. Praesent eu metus laoreet, sodales orci nec, rutrum dui.</p>
              <h4>Inna Rose</h4>
              <span>Google</span>
            </div>
            <div class="client-quote" id="client-quote-7">
              <p>Sed ornare enim ligula, id imperdiet urna laoreet eu. Praesent eu metus laoreet, sodales orci nec, rutrum dui.</p>
              <h4>Jacob Nelson</h4>
              <span>Support</span>
            </div>
            <div class="client-quote" id="client-quote-8">
              <p>Adipiscing elit euismod tincidunt ut laoreet dolore magna aliquam dolor sit amet consectetuer elit</p>
              <h4>John Doe</h4>
              <span>Marketing</span>
            </div>
            <div class="client-quote" id="client-quote-9">
              <p>Nam euismod fringilla turpis vitae tincidunt, adipiscing elit euismod tincidunt aliquam dolor sit amet consectetuer elit</p>
              <h4>Michael Stawson</h4>
              <span>Graphic Designer</span>
            </div>
            <div class="client-quote" id="client-quote-10">
              <p>Quisque eget mi non enim efficitur fermentum id at purus.</p>
              <h4>Liam Nelsson</h4>
              <span>Actor</span>
            </div>
            <div class="client-quote" id="client-quote-11">
              <p>Integer et ante dictum, hendrerit metus eget, ornare massa.</p>
              <h4>Madison Klarsson</h4>
              <span>Director</span>
            </div>
            <div class="client-quote" id="client-quote-12">
              <p>Vestibulum sodales imperdiet euismod.</p>
              <h4>Ava Veen</h4>
              <span>Writer</span>
            </div>
            <div class="client-quote" id="client-quote-13">
              <p>Ut sit amet nisl nec dui lobortis gravida ut et neque. Praesent eu metus laoreet, sodales orci nec, rutrum dui.</p>
              <h4>Sophia Williams</h4>
              <span>Apple</span>
            </div>
            <div class="client-quote" id="client-quote-14">
              <p>Nam non vulputate orci. Duis sed mi nec ligula tristique semper vitae pretium nisi. Pellentesque nec enim vel magna pulvinar vulputate.</p>
              <h4>Melissa Korn</h4>
              <span>Reporter</span>
            </div>
          </div>
        </div>
        <!-- End Clients Quotes -->
      </div>
    </section>
    <!-- END CLIENTS SECTION -->