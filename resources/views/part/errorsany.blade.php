@if($errors->any())
  
    
  @foreach ($errors->all() as $error)
      <script>toastr.warning("{{ $error }}")</script>
  @endforeach
   
  </div>


@endif

@if(session()->has('success'))
 <script>toastr.success("{{session('success')}}")</script>
@endif

@if(session()->has('warning'))
 <script>toastr.warning("{{session('warning')}}")</script>
@endif