<div id="event" style="margin-bottom: 50px">
  
</div>
<section >
  
  <div class="container service-bg">
    <div class="heading">
      <h2><strong>EVENT</strong> </h2>
      <p>The Most Recent Events</p>
    </div>
    <!-- //end heading -->
    <div class="row">
@php
$partiescount=0;
@endphp
@foreach($parties as $party)
<?php 
$fecha = new Carbon\Carbon($party->dstart);
$fecha2 = new Carbon\Carbon($party->dend);      

?> 
@php
$partiescount++;
@endphp
    
{{-- la primera carta  son 3 --}}
      <div class="col-sm-4" 
      style="min-height:300px;
      margin-top: 25px;
    
      padding: 20px;
      ">
        <div class="margin-bottom-100">
          <div >
            <div >
              <div class="row" style="min-height: 165px">
                <div class="col-sm-6">
                  {{-- aqui va la imagen' --}}
                  <div>                
                    <img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive"  style="width: 100%;height: 100%;">
                  </div>
                </div>
                {{-- aqui van los textos --}}
                <div class="col-sm-6">
                <h3 class="color-black">{{$party->name}}</h3>
                      <p  class="color-black"><span class="glyphicon glyphicon-calendar "> </span>
                <span  class="color-black">  
                       {{$fecha->day}}  {{substr( $fecha->format('F') ,0 , 3) }} -- {{$fecha2->day}}  {{substr( $fecha2->format('F') ,0 , 3) }}  <br>
                       {{$party->address}}
                      <br>
                </span>  
                
          </p>
      

            
           
              </div>

              </div>
              {{-- aqui va la description --}}
              <div class="row">
                <div class="col-sm-12">
                  <br>
                   {{$party->description}} <br>
                  
                </div>
              </div>
        

              </div>
             
            </div>
          </div>
          @if($party->web!="")
              @if(stristr($party->web,"http://") || stristr($party->web,"https://"))
              <div style="position: absolute;bottom: 0px;margin-left: auto;margin-right:auto;left:0;right:0;">                
                <div style="text-align: center;">
               <a href="{{$party->web}}" target="_blank"   class="color-black">{{$party->web}}</a>
            </div>
          </div>
              @else
              <div style="position: absolute;bottom: 0px;margin-left: auto;margin-right:auto;left:0;right:0;">                
                <div style="text-align: center;">
                  
               <a href="http://{{$party->web}}" target="_blank"  class="color-black" >{{$party->web}}</a>
               <br><br>
                </div>
                
              </div>
              @endif
            @endif
      </div>
        @if($partiescount%3==0) 
      </div>
      <div class="row" >
        @endif 
        @if($partiescount >= 6) 
        @break
        @endif
@endforeach  
    </div>
<center><a href="http://eventr7m.ideveloper.technology/server.php#calendar" class="btn-brd-danger" style="margin-top: 50px;">View More</a></center>
  </div>
  <!-- Services END -->
</section>
<!-- END ABOUT SECTION -->