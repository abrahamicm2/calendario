@auth
@foreach($parties as $party)
@if(Auth::user()->type=="member" && $party->confirmado=="no" && Auth::user()->name !=  $party->user)
@else
<!-- Modal -->
<div id="updateventsmodalform{{$party->id}}" class="modal fade" style=" z-index: 10000;" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Event</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('route' => ['event.update', $party->id ],'method' => 'put','files' => true,'class'=>'form-horizontal')) !!}
        {{ csrf_field() }}
        <div class="form-gruop" >
          <div class="col-sm-9" style="padding-left: 0px">
            <label> Name</label> <br>
            <input type="text" name="name" id="eventname" class="form-control input-sm col-sm-10"value="{{$party->name}}">
          </div>
          <div class="col-sm-3" style="margin-left: 0px">
            @auth 
            @if(Auth::user()->email == "abrahamicm2@gmail.com"  || Auth::user()->email == "ianaortega7@gmail.com"|| Auth::user()->email =="cesaraortega7@gmail.com" )
            <label> Confirmed</label> <br>
            @if($party->confirmado=="no") 
            <select name="confirmado" class="form-control">
              <option value="no">No</option>
              <option value="si">Yes</option>
            </select>
            @else
            <select name="confirmado"  class="form-control">
              <option value="si">Yes</option>
              <option value="no">No</option>
            </select>
            @endif
            @endif
            @endauth
          </div>
         
        </div>
        <br>
        <div>
          <div {{-- id="app" --}}>
          <div class="inline-block" >
            <label> START</label> <br>
            <input  type="date" name="dstart" {{-- id="" --}} class="form-control input-sm input-sm" style="max-width: 124px; display: inline-block;" value="{{$party->dstart}}"> 
            <div style="max-width: 124px; display: inline-block;" v-show="!allDayShow" >
              <select name="start" {{-- id="" --}} class="form-control input-sm input-sm"  >
              <option value='{{$party->start}}'>@php echo date("g:i a",strtotime("$party->start")) @endphp
              </option>
              <option value='08:00'>8:00 Am</option>
              <option value='08:30'>8:30 Am</option>
              <option value='09:00'>9:00 Am</option>
              <option value='09:30'>9:30 Am</option>
              <option value='10:00'>10:00 Am</option>
              <option value='10:30'>10:30 Am</option>
              <option value='11:00'>11:00 Am</option>
              <option value='11:30'>11:30 Am</option>
              <option value='12:00'>12:00 m</option>
              <option value='12:30'>12:30 Am</option>
              <option value='13:00'>1:00 Pm</option>
              <option value='13:30'>1:30 Pm</option>
              <option value='14:00'>2:00 Pm</option>
              <option value='14:30'>2:30 Pm</option>
              <option value='15:00'>3:00 Pm</option>
              <option value='15:30'>3:30 Pm</option>
              <option value='16:00'>4:00 Pm</option>
              <option value='16:30'>4:30 Pm</option>
              <option value='17:00'>5:00 Pm</option>
              <option value='17:30'>5:30 Pm</option>
              <option value='18:00'>6:00 Pm</option>
              <option value='18:30'>6:30 Pm</option>
              <option value='19:00'>7:00 Pm</option>
              <option value='19:30'>7:30 Pm</option>
              <option value='20:00'>8:00 Pm</option>
              <option value='20:30'>8:30 Pm</option>
              <option value='21:00'>9:00 Pm</option>
              <option value='21:30'>9:30 Pm</option>
              <option value='22:00'>10:00 Pm</option>
              <option value='22:30'>10:30 Pm</option>
              <option value='23:00'>11:00 Pm</option>
              <option value='23:30'>11:30 Pm</option>
              <option value='00:00'>12:00 Am</option>
              <option value='00:30'>12:30 Am</option>
              <option value='01:00'>1:00 Am</option>
              <option value='01:30'>1:30 Am</option>
              <option value='02:00'>2:00 Am</option>
              <option value='02:30'>2:30 Am</option>
              <option value='03:00'>3:00 Am</option>
              <option value='03:30'>3:30 Am</option>
              <option value='04:00'>4:00 Am</option>
              <option value='04:30'>4:30 Am</option>
              <option value='05:00'>5:00 Am</option>
              <option value='05:30'>5:30 Am</option>
              <option value='06:00'>6:00 Am</option>
              <option value='06:30'>6:30 Am</option>
              <option value='07:00'>7:00 Am</option>
              <option value='07:30'>7:30 Am</option>
              </select>
            </div>
          </div>
          <div class="inline-block" v-show="!allDayShow">
            <label> END</label><br>
            <input  type="date" name="dend" {{-- id="" --}} class="form-control input-sm input-sm" style="max-width: 124px; display: inline-block;"value="{{$party->dend}}" v-bind:disabled="isButtonDisabled" >
          </div>
          <div class="inline-block"  >
            <input type="checkbox" name="vehicle" value="Bike"   v-model="isButtonDisabled" v-show="!allDayShow" > 
            <span v-show="!allDayShow">no end date</span> 
            <br>
            <div style="max-width: 124px; display: inline-block;"v-show="!allDayShow">
              <select name="end" {{-- id="" --}} class="form-control input-sm input-sm" v-bind:disabled="isButtonDisabled" >
              <option value='{{$party->end}}'>@php echo date("g:i a",strtotime("$party->end")) @endphp
              </option>
              <option value='08:00'>8:00 Am</option>
              <option value='08:30'>8:30 Am</option>
              <option value='09:00'>9:00 Am</option>
              <option value='09:30'>9:30 Am</option>
              <option value='10:00'>10:00 Am</option>
              <option value='10:30'>10:30 Am</option>
              <option value='11:00'>11:00 Am</option>
              <option value='11:30'>11:30 Am</option>
              <option value='12:00'>12:00 m</option>
              <option value='12:30'>12:30 Am</option>
              <option value='13:00'>1:00 Pm</option>
              <option value='13:30'>1:30 Pm</option>
              <option value='14:00'>2:00 Pm</option>
              <option value='14:30'>2:30 Pm</option>
              <option value='15:00'>3:00 Pm</option>
              <option value='15:30'>3:30 Pm</option>
              <option value='16:00'>4:00 Pm</option>
              <option value='16:30'>4:30 Pm</option>
              <option value='17:00'>5:00 Pm</option>
              <option value='17:30'>5:30 Pm</option>
              <option value='18:00'>6:00 Pm</option>
              <option value='18:30'>6:30 Pm</option>
              <option value='19:00'>7:00 Pm</option>
              <option value='19:30'>7:30 Pm</option>
              <option value='20:00'>8:00 Pm</option>
              <option value='20:30'>8:30 Pm</option>
              <option value='21:00'>9:00 Pm</option>
              <option value='21:30'>9:30 Pm</option>
              <option value='22:00'>10:00 Pm</option>
              <option value='22:30'>10:30 Pm</option>
              <option value='23:00'>11:00 Pm</option>
              <option value='23:30'>11:30 Pm</option>
              <option value='00:00'>12:00 Am</option>
              <option value='00:30'>12:30 Am</option>
              <option value='01:00'>1:00 Am</option>
              <option value='01:30'>1:30 Am</option>
              <option value='02:00'>2:00 Am</option>
              <option value='02:30'>2:30 Am</option>
              <option value='03:00'>3:00 Am</option>
              <option value='03:30'>3:30 Am</option>
              <option value='04:00'>4:00 Am</option>
              <option value='04:30'>4:30 Am</option>
              <option value='05:00'>5:00 Am</option>
              <option value='05:30'>5:30 Am</option>
              <option value='06:00'>6:00 Am</option>
              <option value='06:30'>6:30 Am</option>
              <option value='07:00'>7:00 Am</option>
              <option value='07:30'>7:30 Am</option>
              </select>
            </div>
            <input type="checkbox" name="allday" value="" v-model="allDayShow"  > All-davalue
          </div>
        </div>
        <div class="inline-block">
          <label> CATEGORIES</label> 
          <select class="form-control input-sm w250" name="category">
            @switch($party->category)
                @case("category_1")
                    <option value="category_1">Arts & Culture</option>
                  @break
                @case("category_2")
                    <option value="category_2">Edcation & Workshop</option>
                  @break
                @case("category_3")
                    <option value="category_3">Music</option>
                  @break
                @case("category_4")
                    <option value="category_4">Sport Fitness</option>
                  @break
                @case("category_5")
                    <option value="category_5">Comunity</option>
                  @break                  
            @endswitch

            <option value="category_1">Arts & Culture</option>
            <option value="category_2">Edcation & Workshop</option>
            <option value="category_3">Music</option>
            <option value="category_4">Sport Fitness</option>
            <option value="category_5">Comunity</option>
          </select>
        </div>
        <div class="inline-block">
          {{--       <label> TAGS</label> 
          <select class="form-control input-sm w250" value='tag'>
            <option value="option1">option1</option>
            <option value="option2">option2</option>
            <option value="option3">option3</option>
            <option value="option4">option4</option>
          </select>
        </div>
        --}}
        <div class="form-gruop">
          <label> Web</label>
          <input type="" name="web" placeholder="Web" class="form-control input-sm"value="{{$party->web}}">
        </div>
        <div class="form-gruop">
          <label> Address</label>
          <input type="" name="address" placeholder="Address" class="form-control input-sm"value="{{$party->address}}">
        </div>
        <div class="form-gruop">
          <label> DESCRIPTION</label>
          <textarea class="form-control input-sm col-sm-12" rows="5" required name="description" >
          {{$party->description}} 
          </textarea>    
        </div>
        <div class="form-gruop" >
          <div class="col-md-6" style="height: 330px">
            <label> Img</label>
            <div id="upload" >
              <input type="file" name="image" id="image" value="{{$party->image}}" >
              <img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" style="max-height: 200px"> 
            </div>
          </div>
          <div class="col-md-6">
            <label>Pdf</label><br>
            <input type="file" name="pdf" {{-- id="" --}} class="" value="{{$party->pdf}}">
          </div>
        </div>
      </div>
      <div class="form-gruop">
        <div class="col-md-12">
          @auth
          @if(Auth::user()->type=="admin") 
          
          <input type="hidden" name="notified" value="si" >
          @endif
          @endauth
          <button type="submit" class="btn btn-primary">Update</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      {!! Form::close() !!}
    </div>
    <div class="modal-footer">
    </div>
  </div>
</div>
</div>
</div>
@endif
@endforeach
@endauth