@auth 
  @if(Auth::user()->type=="admin") <!-- notifications -->
    @if($parties->where('notified', "no")->count()!=0)
    <div style="width: 80%; margin: 0 auto">
        <h3>EVENT NOTIFICATIONS</h3>
    
    <br>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr> <th>Name</th> <th>Imagen</th> <th>Category</th> <th>Description</th> <th>Address</th> <th>Web</th> <th>Date Start</th> <th>Date End</th> <th>Status</th> <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
        @foreach($parties as $party)
          @if($party->notified=="no")
            {!! Form::open(array('route' => ['event.update', $party->id ],'method' => 'put','class' => 'inline-form')) !!}
              {{ csrf_field() }}
              <td> <input type="text" name="name" value="{{ $party->name }}"></td>
              <td><img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive img-rounded"  style="width:204px;height:auto;"></td>
              <td>
                <select  name="category" >
                  <option value="category_1"  @if($party->category=="category 1") selected >category 1  @endif </option> 
                  <option value="category_2" @if($party->category=="category 2") selected @endif >category 2 </option>
                  <option value="category_3" @if($party->category=="category 3") selected @endif >category 3 </option>
                </select>
              </td>
              <td> <input type="text" name="description" value="{{ $party->description}}"></td>
              <td> <input type="text" name="address" value="{{ $party->address}}"></td>
              <td> <input type="text" name="web" value="{{ $party->web}}"></td>
              <td><input type="date" name="dstart" value="{{$party->dstart}}"><input type="time" name="start" value="{{$party->start}}">  </td>
              <td><input type="date" name="dend" value="{{$party->dend}}"><input type="time" name="end" value="{{$party->end}}">  </td>
              <td>
                <select  name="confirmado" >
                  @if($party->confirmado=="no") 
                  <option value="no" >no</option>
                  <option value="si" >yes</option>
                  @else
                  <option value="si">yes</option>
                  <option value="no">no</option>
                  @endif
                </select>
              </td>
              <td>
                @if(Auth::user()->email=="abrahamicm2@gmail.com" || Auth::user()->email=="ianaortega7@gmail.com" || || Auth::user()->email=="cesaraortega7@gmail.com")
                <input type="hidden" name="notified" value="yes">
                @endif
                @if(Auth::user()->type=="admin")
                <button type="submit" class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-edit"></span>
                </button>
            {!! Form::close() !!} <!-- form utdate party -->
            {!! Form::open(array('route' => ['event.destroy', $party->id ],'method' => 'delete','class' => 'inline-form')) !!}
              {{ csrf_field() }}
              <button type="submit"  class="btn btn-warning btn-sm">
              <span class="glyphicon glyphicon-trash"></span>
              </button>
            {!! Form::close() !!}
                @endif <!-- (Auth::user()->type=="admin") -->
              @if($party->pdf!="Null")
              <a class="btn btn-default btn-sm"
                href="/storage/app/{{$party->pdf}}"
                download="{{$party->description}}.pdf"
                >
              <span class="glyphicon glyphicon-download"></span>
              </a>
            </td>
              @endif
          </tr>
          @endif
          @endforeach
          @endif
        </tbody>
      </table>
 </div>   
  </div> 
    @endif
@endauth