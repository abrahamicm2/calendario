
<!-- start calendar   -->


  <div id='calendar' class="container" >
    <div id="scuared">
      <div class="flex-center">
@php
$countpartiesscuared=0;
@endphp
@foreach($parties as $party)
<?php 
$fecha = new Carbon\Carbon($party->dstart);
$fecha2 = new Carbon\Carbon($party->dend);      

?>    
 @if($party->confirmado=="si")
 @php
 $countpartiesscuared++;
 @endphp
        <div class="card-1  item-start" data-toggle="modal" data-target="#myModalParty{{$party->id}}">
          <div class="cont">
            <div class="flex-center h-120">
              <h3  class="category item-start {{$party->category}}">{{$party->name}}</h3>
              <div class="contnum item-start">
                <div class="{{$party->category}}b">
                  {{--$fecha->format('l jS \\of F Y h:i:s A')--}}
                  <div class="month  {{$party->category}}b"> {{substr( $fecha->format('F') ,0 , 3) }}</div>
                  <div class="day  {{$party->category}}b"> {{$fecha->day}}</div>
                  <div class="week  {{$party->category}}b"> {{substr( $fecha->format('l') , 0 , 3)}}</div>
                </div>
                <div class="{{$party->category}}b" >
                  <div class="month {{$party->category}}b"> {{substr( $fecha2->format('F') ,0 , 3) }}</div>
                  <div class="day {{$party->category}}b"> {{$fecha2->day}}</div>
                  <div class="week {{$party->category}}b"> {{substr( $fecha2->format('l') , 0 , 3)}}</div>
                </div>
              </div>
            </div>
            <p>
              <img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive">
            </p>
            <p class="text-center">{{$party->description}}</p>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModalParty{{$party->id}}" role="dialog">
          <div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11';
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
          </script>
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{$party->name}}</h4>
              </div>
              <div class="modal-body">
                <div class="flex-center">
                  <img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive">
                </div>
                <p>
                  <strong>Event Name: </strong>{{$party->name}} <br> 
                  <strong>Description: </strong>{{$party->description}} <br> 
                  <strong>Date: </strong>
                  @if($fecha==$fecha2)
                  {{$fecha->day}}  {{substr( $fecha->format('F') ,0 , 3) }}
                  @else
                  {{$fecha->day}}  {{substr( $fecha->format('F') ,0 , 3) }} -- {{$fecha2->day}}  {{substr( $fecha2->format('F') ,0 , 3) }}
                  @endif


                   <br>
                  @if($party->web!="")
                    @if(stristr($party->web,"http://") || stristr($party->web,"https://"))
                    <strong>Web Site: </strong> <a href="{{$party->web}}" target="_blank">{{$party->web}}</a>
                    @else
                      <strong>Web Site: </strong> <a href="http://{{$party->web}}" target="_blank">{{$party->web}}</a>
                    @endif
                  @endif

                 </p>
              </div>
              <div class="modal-footer">

  @include('part.calendar.buttonsared')
     
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
@endif
@endforeach

@while($countpartiesscuared%4 !=0)
          <div style="width: 270px; background-color: black"> </div>
          @php
            $countpartiesscuared++;
  echo " <script type='text/javascript'> console.log( 'contador countpartiescuared: ".  $countpartiesscuared . "')</script>";

          @endphp
        @endwhile
      </div>
    </div>


   
    <div class="container" id="list" style="padding: 0px">
      <div class="row" >
@foreach($parties as $party)
 @if($party->confirmado=="si")
<?php 
$fecha = new Carbon\Carbon($party->dstart);
$fecha2 = new Carbon\Carbon($party->dend);      
?>  
        <div class="col-md-1"> 
          <img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive" class="img-responsive">
        </div>
        <div class="col-md-11">
          <h3 class="{{$party->category}}">{{$party->name}}</h3>
          <p><span class="glyphicon glyphicon-calendar "> </span> {{$fecha->day}}  {{substr( $fecha->format('F') ,0 , 3) }} -- {{$fecha2->day}}  {{substr( $fecha2->format('F') ,0 , 3) }} | {{$party->address}}</p>
          <p>{{$party->description}}</p>
        </div>
  @endif
@endforeach
      </div>
    </div>
  </div>
</div>
</div>
<!-- end calendars -->