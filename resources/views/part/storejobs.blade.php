
@auth  
  <!-- create event -->
  <div class="container" id="form_jobs_store"  >
    <!--  job  edit-->
    <div class="events-store"  >
      <span class="close" id="span_job_create" >×</span>
      {!! Form::open(array('route' => 'jobs.store','method' => 'post','files' => true,'class'=>'form-horizontal')) !!}
      {{ csrf_field() }}
      <!-- width: 49.8%;  width: 50.8% margin-left: -1%; -->
      <div class="form-group form">
        <input type="hidden" name="pdf" value="null">
        <input type="text" class="form-control" id="title" 
          placeholder="Title Here" name="title" required>
        <input type="date" class="form-control" id="dstart" name="dstart" style="display: inline;width: 49%;" required value="{{$date1->format('Y-m-d')}}"> 
        <input type="time" class="form-control" id="start" name="start" style="display: inline;
          width: 49%" value="08:00"> 
        <input type="date" class="form-control" id="dend" name="dend" style="display: inline;width: 49%;" required value="{{$date2->format('Y-m-d')}}">
        <input type="time" class="form-control" id="end" name="end" style="display: inline;width: 49%" value="08:00">
        <input type="text" class="form-control" id="description" placeholder="Description Here:" name="description" required>
        <select class="form-control" name="category"  required>
          <option >Select Category</option>
          <option value="Sales">Sales</option>
          <option value="Computing">Computing</option>
          <option value="Admiistration">Admiistration</option>
          <option value="Manofacture">Manofacture</option>
        </select>
        <label for="image" class="form-control">Image</label>
        <input type="file"  id="image" name="image"  required class="form-control"> 
        <input type="text" class="form-control" id="address" placeholder="Address Here" name="address" required>
        <input type="text" class="form-control" id="web" placeholder="website Link" name="web">
        @if(Auth::user()->type=="admin")
        <input type="hidden" name="confirmed" value="yes" >
        <input type="hidden" name="notified" value="yes" >
        @endif
        <button type="submit" class="btn btn-danger" style="
          color: #fff;
          background-color: #337ab7;
          border: 2px solid #337ab7 !important;
          ">Create</button>
        <a  class="btn btn-danger" id="jobcancel">Cancel</a>
        {!! Form::close() !!}
      </div>
    </div>  
  </div>
@endauth