@auth 
  @if(Auth::user()->type=="admin") <!-- notifications -->
    @if($parties->where('notified', "no")->count()==0 && $jobs->where('notified', "no")->count()==0 )
    <div class="row" >
      <div class="alert alert-success text-center">
        <p class="text-center">There not notifications</p>
      </div>
    </div>
    @endif
  @endif
@endauth