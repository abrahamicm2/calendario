
@auth  
  @if(Auth::user()->type=="admin") <!-- notifications -->
    @if($jobs->where('notified', "no")->count()!=0)
     <div style="width: 80%; margin: 0 auto">
      <h3>JOBS Notifications</h3>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr> <th>Title</th> <th>Image</th> <th>Category</th> <th>Web</th> <th>Address</th> <th> Date Start</th> <th> Date End</th> <th>Description</th> <th>Status</th> <th>Action</th>
          </tr>
        </thead>
        <tbody>
      @foreach($jobs as $job)
        @if($job->notified=="no")
          <tr>
            {!! Form::open(array('route' => ['jobs.update', $job->id ],'method' => 'put','class' => 'inline-form')) !!}
              {{ csrf_field() }}
              <input type="hidden" name="pdf" value="null">
              <td><input type="text" name="title" value="{{ $job->title}}"> </td>
              <td><img src="http://eventr7m.ideveloper.technology/storage/app/{{$job->image}}" class="img-responsive img-rounded" style="width:204px;height:auto;"></td>
              <td><select name="category" >
                <option value="category_1" @if($job->category== 'category 1') selected @endif > category 1</option>
                <option value="category_2" @if($job->category== 'category 2') selected @endif > category 2</option>
                <option value="category_3" @if($job->category== 'category 3') selected @endif > category 3</option>
                <option value="category_4" @if($job->category== 'category 4') selected @endif > category 4</option>
                </select></td>
              <td> <input type="text" name="address" value="{{ $job->address}}"></td>
              <td> <input type="text" name="web" value="{{ $job->web}}"></td>
              <td><input type="date" name="dstart" value="{{$job->dstart}}"><input type="time" name="start" value="{{$job->start}}">  </td>
              <td><input type="date" name="dend" value="{{$job->dend}}"><input type="time" name="end" value="{{$job->end}}">  </td>
              <td><input type="text" name="description" value="{{ $job->description }}"></td>
              <input type="hidden" name="notified" value="yes">
              <td>
                @if($job->confirmed=="no") 
                <select name="confirmed" > <option value="no">No</option> <option value="yes">Yes</option></select>
                @else
                <select name="confirmed" > <option value="yes">Yes</option> <option value="no">No</option  </select>
              </td>
                @endif
              <td>
                <button type="submit"  class="btn btn-success btn-sm">
                <span class="glyphicon glyphicon-edit"></span>
                </button>
              {!! Form::close() !!}
              {!! Form::open(array('route' => ['jobs.destroy', $job->id ],'method' => 'delete','class' => 'inline-form')) !!}
                {{ csrf_field() }}
                <button type="submit"  class="btn btn-warning btn-sm">
                <span class="glyphicon glyphicon-trash"></span>
                </button>
              {!! Form::close() !!}
              </td>
          </tr>
        @endif      
      @endforeach
    @endif
        </tbody>
      </table>
    </div>
  </div> <!-- 956 872 -->   <!-- end jobs dasboard --> 
  </div>
  @endif
@endauth