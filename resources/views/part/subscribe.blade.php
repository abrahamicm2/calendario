<!-- SUBSCRIBE BEGIN -->
<div class="subscribe">
  <div class="container">
    <div class="subscribe-wrap">
      <div class="subscribe-body subscribe-desc md-margin-bottom-30">
        <h1>Signup for free</h1>
        <p>Publish your Events or job opportunities <br>Free access for our partners</p>
      </div>
      <div class="subscribe-body">
        <form class="form-wrap input-field">
          <div class="form-wrap-group">
            <input type="text" class="form-control2"  placeholder="Name">
          </div>
          <div class="form-wrap-group border-left-transparent">
            <input type="email" class="form-control2"  placeholder="Your Email">
          </div>
          <div class="form-wrap-group">
            <button type="submit" class="btn-danger btn-md btn-block">Signup</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- SUBSCRIBE END -->