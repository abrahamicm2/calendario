@auth

@if(Auth::user()->type=="member")
@else
<!-- Modal -->
<div id="modalnotifications" class="modal" >
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">notifications</h4>
      </div>
      <div class="modal-body">
        <h5>  Events</h5>
             <table style="width:100%">
  <tr>
    <th>User   </th>
    <th>Name Event: </th> 
    <th>Description</th>
    <th>View More</th>

  </tr>
        @foreach($parties as $party)
        @if($party->notified=="no")
  <tr>  
    <td>{{$party->user}}</td>
    <td> {{$party->name}}</td> 
    <td>{{$party->description}}</td>
    <td>
      <a  href="http://eventr7m.ideveloper.technology/server.php#notyevents" >
      <span class="glyphicon glyphicon-edit" ></span>
      </a>

    </td>
  </tr>
  
    @endif
        @endforeach
</table>

        <h5>  Jobs</h5>
             <table style="width:100%">
  <tr>
    <th>User   </th>
    <th>Title Job: </th> 
    <th>Description</th>
    <th>View More</th>
  </tr>
        @foreach($jobs as $job)
        @if($job->notified=="no")
  <tr>  
    <td>{{$job->user}}</td>
    <td> {{$job->title}}</td> 
    <td>{{$job->description}}</td>
    <td>
      <a  href="http://eventr7m.ideveloper.technology/server.php#notyjobs" >
      <span class="glyphicon glyphicon-edit"></span>
      </a>
    </td>
  </tr>
  
    @endif

        @endforeach
</table>




          
       
     
    </div>
    <div class="modal-footer">
     
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
</div>
@endif

@endauth