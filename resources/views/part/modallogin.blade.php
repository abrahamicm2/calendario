    <!-- The Modal  Login-->
    <div id="myModal_login" class="modal">
      <!-- Modal content -->
      <div class="modal_content cregister ">
        <a   id="bt_login_close" class="close">×</a>
        {!! Form::open(array('url' => 'login','method' => 'post','class'=>'col-md-9' )) !!}
          {{ csrf_field() }}
          <div class="form-group " style="background: #2c3a46;">
            <input type="email" class=" form-control2" placeholder="Email" name="email">
            <input type="password" class=" form-control2" placeholder="Password" name="password">
            <button type="submit" class="btn-danger btn-md btn-block">Login</button>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
    <!-- The Modal  Login-->