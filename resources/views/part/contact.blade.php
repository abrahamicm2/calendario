

  <!-- BEGIN CONTACT SECTION -->
  
  <section id="contact">
    <!-- Footer -->
    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="heading-left-light">
              <h2>CITY CONNECTIONS</h2>
              <p>10411 W. Markham Street , Suite 100 <br> Little Rock, Arkansas 72205</p>
              <p><br>Office: (501) 376-1686 <br> Fax: 501-325-7480 </p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form">
              <div class="form-wrap" style="height:auto">
                <div class="form-wrap-group">
                  <input type="text" placeholder="Your Name" class="form-control3">
                  <input type="text" placeholder="Subject" class="border-top-transparent form-control3">
                </div>
                <div class="form-wrap-group border-left-transparent">
                  <input type="text" placeholder="Your Email" class="form-control3">
                  <input type="text" placeholder="Contact Phone" class="border-top-transparent form-control3">
                </div>
              </div>
            </div>
            <textarea rows="8" name="message" placeholder="Write comment here ..." class="border-top-transparent form-control3"></textarea>
            <button type="submit" class="btn-danger btn-md btn-block">Send it</button>
          </div>
        </div>
        <!-- //end row -->
      </div>
    </div>
    <!-- End Footer -->
    <!-- <canvas id="the-canvas2"></canvas> -->
    <!-- Footer Coypright -->
    <div class="footer-copyright">
      <div class="container">
        <h3>GET CONNECTED</h3>
        <!--<ul class="copyright-socials">
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul> -->
        <P>Designed with by <a href="http://www.rock7.media">Rock 7 Media</a></P>
      </div>
    </div>
    <!-- End Footer Coypright -->
  </section>
  <!-- END CONTACT SECTION -->