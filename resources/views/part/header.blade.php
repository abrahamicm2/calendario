 <header class="page-header">
      <nav class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="toggle-icon">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </span>
            </button>
            <a class="navbar-brand" href="http://cityconnectionsinc.org/">
            <img class="logo-default" src="public/assets/onepage2/img/logo_default.png" alt="Logo">
            <img class="logo-scroll" src="public/assets/onepage2/img/logo_scroll.png" alt="Logo">
            </a>
          </div>
          

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-responsive-collapse">
            <ul class="nav navbar-nav">
              <li class="page-scroll active">
                <a href="#intro">Home</a>
              </li>
              <li class="page-scroll">
                <a href="http://eventr7m.ideveloper.technology/server.php#event">Event</a>
              </li>
              <li class="page-scroll">
                <a href="http://eventr7m.ideveloper.technology/server.php#jobs">Jobs</a>
              </li>
              <li class="page-scroll">
                <a href="http://eventr7m.ideveloper.technology/server.php#contact">Contact</a>
              </li>
@auth
              <li class="page-scroll" style="width: 185.97px;"  >
              <a href="" style="text-align: right;" id="snlogin"> <span uk-icon="icon: user; ratio: 2" class="menu-login"></span></a>
               <div style="padding: 20px;background: #fff; margin-top: 10px;" id="nlogin" >
                <ul style=" list-style: none;" class="nlogin">
               
                 
                  <li style="padding: 0px">
                    <a style="color: #4d749d;padding:5px 0;    text-decoration: none;" data-toggle="modal" data-target="#updateuser{{Auth::user()->id}}" data-backdrop="false">
                      <i class="fa fa-user"></i>
                    My Account</a></li> 
                  <li style="padding: 0px">
                    <a href="" style="color: #4d749d;padding:5px 0;text-decoration: none;" onclick="event.preventDefault();document.getElementById('logout-form').submit();">


                      <i class="fa fa-sign-out" ></i>
                   Logout 
                 </a>
                 <form id="logout-form" action="{{route('logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
                 </form>

               </li></ul>
                </div>
 

                  

                 
               
             </li>
              <li>
               
@endauth
@guest
              <li class="page-scroll">
                <!-- <a href="{{url('home')}}">Register</a> -->
                <a  id="bt_register">Register</a>
              </li>
              <li class="page-scroll">
                <!-- <a href="{{url('home')}}" id="bt-login">Login</a> -->
                <a  id="bt_login">Login</a>
              </li>
@endguest
            </ul>
          </div>
          <!-- End Navbar Collapse -->
        </div>
        <!--/container-->
      </nav>
    </header>