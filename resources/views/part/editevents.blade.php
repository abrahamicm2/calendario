
@auth
@if(Auth::user()->type=="admin") {{-- si el usuario es admin se muestra si no se me=uestra la plantilla user --}}
  <div class="row">
    <!-- event index 973 -->
    <div class="col-md-12" style="padding: 60px">
      <div class="row">
        <div class="col-md-9">
          <h3>EVENT MANAGEMENT</h3>
          <!-- Trigger the modal with a button -->


          <a type="button" class="btn btn-success" id="newevents">
           <span class="glyphicon glyphicon-plus " ></span> New</a>                
        </div>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Imagen</th>
              <th>Category</th>
              <th>Description</th>
              <th>Address</th>
              <th>Web</th>
              <th>Date Start</th>
              <th>Date End</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach($parties as $party)
              @if(Auth::user()->type=="member" && $party->confirmado=="no" && Auth::user()->name !=  $party->user)
              @else
            <tr  @if($party->notified=="no") class="bg-danger" @endif>
              @if(Auth::user()->type=="admin")
              <td> {{ $party->name }}</td>
              <td><img src="http://eventr7m.ideveloper.technology/storage/app/{{$party->image}}" class="img-responsive img-rounded"   style="width: 100px;height: 100px"></td>
              <td>
                @switch($party->category)
                    @case("category_1")
                        Arts & Culture
                      @break
                    @case("category_2")
                        Edcation & Workshop
                      @break
                    @case("category_3")
                        Music
                      @break
                    @case("category_4")
                        Sport Fitness
                      @break
                    @case("category_5")
                        Comunity
                      @break                  
                @endswitch


              </td>
              <td> {{ $party->description}}</td> 
              <td> {{$party->address}}</td>
              <td> {{ $party->web}}</td>
              <td>{{$party->dstart}} -- {{$party->start}}</td>
              <td>{{$party->dend}}-- {{$party->end}}</td>
              <input type="hidden" name="notified" value="yes">
              <td>
                @if($party->confirmado=="no") 
                  no
                @else
                
                yes
              </td>
              @endif
              <td>
                @if(Auth::user()->type=="admin")
                <button class="btn btn-success btn-sm"  data-toggle="modal" data-target="#updateventsmodalform{{$party->id}}"  data-backdrop="false">
                <span class="glyphicon glyphicon-edit"></span>
                </button>
               
                {!! Form::open(array('route' => ['event.destroy', $party->id ],'method' => 'delete','class' => 'inline-form destroy-event-form')) !!}
                {{ csrf_field() }}
                <button type="submit"  class="btn btn-warning btn-sm destroy-event-submit">
                <span class="glyphicon glyphicon-trash "></span>
                </button>
                {!! Form::close() !!}
                @endif
                @if($party->pdf!="Null")
                <a class="btn btn-default btn-sm"
                  href="/storage/app/{{$party->pdf}}"
                  download="{{$party->description}}.pdf"
                  >
                <span class="glyphicon glyphicon-download"></span>
                </a>
                @else
                <!--  <a class="btn btn-default btn-sm"
                  disabled="true"> 
                  <span class="glyphicon glyphicon-download"></span>
                  </a> -->
              </td>
              @endif
            </tr>
            @endif
            @endif
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- end events dasboard --> 
@endif  {{-- end administrador --}}
@endauth