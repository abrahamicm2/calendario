
@auth
  <!-- users -->
  @if(Auth::user()->type=="admin")
  <div class="row">
    <div class="col-md-12" style="padding: 60px">
      <div class="row">
        <div class="col-md-9">
          <h3>Usuarios</h3>
        </div>
        <!--   <div class="col-md-3">
          <br> <br> <br>
          <div class="input-group" >
             <input type="text" class="form-control" placeholder="Search">
             <span class="input-group-addon"> <span class="glyphicon glyphicon-search"></span>
          </div>
          </div> -->
      </div>
      <br>
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Lastname</th>
              <th>Email</th>
              <th>Type</th>
              <th>  
                Action               
              </th>
            </tr>
          </thead>
          <tbody>
              @foreach ($users as $user) 

{{-- formulario update-user --}}
<!-- Modal updateuser -->
<div id="updateuser{{$user->id}}" class="modal" style="z-index: 10000;" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Users</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('route' => ['admin.users.update', $user->id ],'method' => 'put','class'=>'form-horizontal', 'id' => 'form-user-update'. $user->id)) !!}
        {{ csrf_field() }}
          <div>

      <label> Name</label> 
      <input type="text" name="name" id="eventname" class="form-control input-sm col-sm-10" placeholder="name" value="{{$user->name}}">
    </div>
<br>
<div>
           
    <div class="form-gruop">
      <label> lastname</label>
<input type="" name="lastname" placeholder="lastname" class="form-control input-sm" value="{{$user->lastname}}">
    </div>
       <div class="form-gruop">
      <label> email</label>
<input type="" name="email" placeholder="email" class="form-control input-sm" value="{{$user->email}}">

    </div>
    @if($user->email == "abrahamicm2@gmail.com" || $user->email =="ianaortega7@gmail.com"  || $user->email =="cesaraortega7@gmail.com")
    @if(Auth::user()->id != $user->id )
    <div class="form-gruop">
      <label> Type</label>
      <select name='type' id="type"  class="form-control">
        @if($user->type=="admin")
        <option value="admin">admin</option>
        <option value="member">member</option>
        @else
        <option value="member">member</option>
        <option value="admin">admin</option>
        @endif
       
      </select>
    </div>
    @endif
    @endif
    <br>

<button type="submit" class="btn btn-primary input-sm">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    
  </div>
</div>
  

  {!! Form::close() !!}

  </div>
        <div class="modal-footer">
        </div>
      </div>

    </div>

{{-- fin update-user --}}




            <tr data-id="{{$user->id}}">
              <td>{{ $user->name }}</td>
              <td>{{ $user->lastname }}</td>
              <td>{{ $user->email }}</td>
              <td>
                @if($user->email == "abrahamicm2@gmail.com"  || $user->email == "ianaortega7@gmail.com"|| $user->email =="cesaraortega7@gmail.com" )
                <b>Superadmin</b>
                @else
                
                  @if($user->type=="admin")
                 Admin
                 
                  @else
                 
                 Member
                  @endif
                  @endif
                </select>
              </td>
              <td>
                @if($user->email == "abrahamicm2@gmail.com" || $user->email =="ianaortega7@gmail.com"  || $user->email =="cesaraortega7@gmail.com")
                @else
                <a {{-- href="http://eventr7m.ideveloper.technology/server.php/users/{{ $user->id}}/delete" --}} class="btn btn-warning btn-sm delete-user">
                <span class="glyphicon glyphicon-trash"></span>
                </a>
                <a class="btn btn-success btn-sm update-user" data-toggle="modal" data-target="#updateuser{{$user->id}}" data-backdrop="false">
                <span class="glyphicon glyphicon-edit "></span>
                </a>

                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @endif
  {!!  Form::open(['route' =>  ['admin.users.destroy', ':USER_ID'], 'method' => 'delete', 'id' => 'form-delete']) !!}
  {!! Form::close() !!}
  
 
    </div>
  @endauth 