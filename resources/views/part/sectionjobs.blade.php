<div id="jobs" style="margin-bottom: 50px"></div>
   <!-- BEGIN JOBS SECTION -->
    <section id="jobs">
      <!-- JOBS BEGIN -->
      <div class="container service-bg" style="padding-bottom: 0px;">
          <div class="heading">
            <h2><strong> Jobs </strong> </h2>
            <p>Most Recent job offers</p>
          </div>
          <!-- //end heading -->
          <!-- Features -->
          <div class="row" >
@php
$jobscount=0;
@endphp
@foreach($jobs as $job)
@php
$jobscount++;
$fecha = new Carbon\Carbon($job->dstart);
$fecha2 = new Carbon\Carbon($job->dend);  
@endphp
    <div class="col-sm-4" style="min-height:300px;margin-top: 25px;padding: 20px">
      <div class="margin-bottom-100">
        <div >
          <div>
            <div  class="row" style="min-height: 165px">
              <div class="col-sm-6">
                {{-- aqui va la imagen' --}}
             <div> 
                <img src="http://eventr7m.ideveloper.technology/storage/app/{{$job->image}}" alt="" class="img-responsive img-rounded">
            </div>
              </div>
              {{-- aqui van los textos --}}
              <div class="col-sm-6">                 
              <h3 class="colo-black">{{$job->title}}</h3>
                  <p class="colo-black"><span class="glyphicon glyphicon-calendar "> </span>
                <span  class="color-black"> 
               {{$fecha->day}}  {{substr( $fecha->format('F') ,0 , 3) }} -- {{$fecha2->day}}  {{substr( $fecha2->format('F') ,0 , 3) }} <br> {{$job->address}}
             <br>
           </span>
            </p>

          </div>
        </div> 

       {{--  {{$job->description}}
        <a href="{{$job->web}}">{{$job->web}}</a>  --}}

                {{-- aqui va la description --}}
                <div class="row">
                  <div class="col-sm-12">
                    <br>
                     {{$job->description}} <br>
                    
                  </div>
                </div>
          

                </div>
               
              </div>
            </div>
            @if($job->web!="")
                @if(stristr($job->web,"http://") || stristr($job->web,"https://"))
                <div style="position: absolute;bottom: 0px;margin-left: auto;margin-right:auto;left:0;right:0;">                
                  <div style="text-align: center;">
                 <a href="{{$job->web}}" target="_blank"   class="color-black">{{$job->web}}</a>
              </div>
            </div>
                @else
                <div style="position: absolute;bottom: 0px;margin-left: auto;margin-right:auto;left:0;right:0;">                
                  <div style="text-align: center;">
                    
                 <a href="http://{{$job->web}}" target="_blank"  class="color-black" >{{$job->web}}</a>
                  </div>
                  
                </div>
                @endif
              @endif
        </div>














        @if($jobscount%3==0 && $jobscount<= 5 ) 
      </div>
      <div class="row" >
        @endif   

          @if($jobscount%3==0 && $jobscount >= 6 ) 
        </div>
        <div class="row togglejobs" >
          @endif 


            
@endforeach
          </div>
            <center><a href="#" class="btn-brd-danger" id="viewmorjob" style="margin-bottom: 50px;margin-top: 50px;">View More</a></center>
        </div>
   
      <!-- Features END -->
    </section>
    <!-- END FEATURES SECTION -->
    <!-- jobs dasboard -->
