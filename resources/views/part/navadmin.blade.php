@auth 
  <div class="dasboard">
    <ul>
      <li>
        <a  href="#1" class="glyphicon glyphicon-bell" data-toggle="modal" data-target="#modalnotifications" data-backdrop="false">
  @if($parties->where('notified', "no")->count() + $jobs->where('notified', "no")->count() != 0 && Auth::user()->type=="admin" )
          
          <span style="color: #ff0909;font-weight: 600;" class="spanhover">  
            {{$parties->where('notified', "no")->count() + $jobs->where('notified', "no")->count()}}
          </span>
            
  @endif
        </a>
      </li>
     <li>
{{-- ***************************CREAR EVENTO*********************************--}}
{{-- *************************si el usuario es member  y no tiene eventos creados abre el modal de crear eventos  **************** --}}
      @if(Auth::user()->type=="member" && app\Party::where('user', '=', Auth::user()->name )->count() < 1)
        <li id="licalendar">
           <a class="glyphicon glyphicon-calendar"  ></a>
           <ul class="ulcalendar">
             <li style="margin-bottom: -1px;"><a data-target="#myModal" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Event</a></li>
             <li style="margin-bottom: -1px;"><a style="width: 125px" data-toggle="modal" data-target="#nothereeven" data-backdrop="false">-See Event</a> </li>
           </ul>
        </li>

      @endif

{{-- *************************si el usuario es member  y  tiene un  evento creado  abre el modal de actualizar evento  **************** --}}
      @if(Auth::user()->type=="member" && app\Party::where('user', '=', Auth::user()->name )->count() >= 1)
      @php
      $parti = app\Party::where('user', '=', Auth::user()->name )->first();
      @endphp
      <li id="licalendar">
         <a class="glyphicon glyphicon-calendar"  ></a>
         <ul class="ulcalendar">
           <li style="margin-bottom: -1px;"><a style="width: 125px">-Create Event</a></li>
           <li style="margin-bottom: -1px;"><a style="width: 125px"  data-toggle="modal" data-target="#updateventsmodalform{{$parti->id}}"  data-backdrop="false">-See Event</a> </li>
         </ul>
      </li>


      @endif
{{-- *************************si el usuario es admin  y  crea todos los eventos que quiera  **************** --}}
      @if(Auth::user()->type=="admin")
       <li id="licalendar">
          <a class="glyphicon glyphicon-calendar"  ></a>
          <ul class="ulcalendar">
            <li style="margin-bottom: -1px;"><a data-target="#myModal" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Event</a></li>
            <li style="margin-bottom: -1px;"><a href="http://eventr7m.ideveloper.technology/server.php#notyevents" style="width: 125px">-See Event</a> </li>
          </ul>
       </li>

        
      @endif





     </li>

      <li>
{{-- ***************************CREAR JOB*********************************--}}
{{-- *************************si el usuario es member  y no tiene Jobs creados abre el modal de crear Jobs  **************** --}}
      @if(Auth::user()->type=="member" && app\Job::where('user', '=', Auth::user()->name )->count() < 1)
         <li id="lijob" > 
        <a class="glyphicon glyphicon-list-alt" ></a>
        <ul class="uljob">
          <li style="margin-bottom: -1px;"><a  data-target="#storejobsmodalform" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Job</a></li>
          <li style="margin-bottom: -1px;"><a data-toggle="modal" data-target="#notherejob" data-backdrop="false" style="width: 125px">-See Job</a> </li>
        </ul>
      </li>
      
      @endif      


{{-- *************************si el usuario es member  y  tiene un  job creado  abre el modal de actualizar job  **************** --}}
      @if(Auth::user()->type=="member" && app\Job::where('user', '=', Auth::user()->name )->count() >= 1)
      @php
      $parti = app\Job::where('user', '=', Auth::user()->name )->first();
      @endphp
         <li id="lijob" > 
        <a class="glyphicon glyphicon-list-alt" ></a>
        <ul class="uljob">
          <li style="margin-bottom: -1px;"><a  data-target="#storejobsmodalform" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Job</a></li>
          <li style="margin-bottom: -1px;"><a data-toggle="modal" data-target="#updatejobsmodalform{{$parti->id}}"  data-backdrop="false" style="width: 125px">-See Job</a> </li>
        </ul>
      </li>
      @endif
        
{{-- *************************si el usuario es admin  y  crea todos los Jobs que quiera  **************** --}}
      @if(Auth::user()->type=="admin")
         <li id="lijob" > 
        <a class="glyphicon glyphicon-list-alt" ></a>
        <ul class="uljob">
          <li style="margin-bottom: -1px;"><a  data-target="#storejobsmodalform" data-toggle="modal" data-backdrop="false" style="width: 125px">-Create Job</a></li>
          <li style="margin-bottom: -1px;"><a href="http://eventr7m.ideveloper.technology/server.php#notyjobs" style="width: 125px">-See Job</a> </li>
        </ul>
      </li>
      @endif     

       </li>
      
      
{{-- ***************************ADMINISTRACION DE USUARIOS*********************************--}}

      @if(Auth::user()->type=="admin")
      <li><a href="http://eventr7m.ideveloper.technology/server.php#users1"   class="glyphicon glyphicon-user"></a></li>
      @endif
    </ul>
  </div>

{{--**************************** modales********************************* --}}
<!-- Modal notherejob -->
<div class="modal" id="notherejob" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">No there Jobs created</h4>
      </div>
      <div class="modal-body">
        <p>Please create new Job</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

<!-- Modal nothereeven -->
<div class="modal" id="nothereeven" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">No there Event created</h4>
      </div>
      <div class="modal-body">
        <p>Please create new Events</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>



@endauth