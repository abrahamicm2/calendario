    <div id="myModal_register" class="modal">
      <!-- Modal content -->
      <div class="modal_content">
        <div class="container">
          <div class="row">
            <div class="cregister">
              <span class="close" id="span_register">&times;</span>
              <div class="col-sm-9">
                {!! Form::open(array('url' => 'register','method' => 'post','class'=>'form')) !!}
                  {{ csrf_field() }}
                  <div class="form-wrap">
                    <div class="form-wrap-group">
                      <input type="text" placeholder="Organization" class="form-control2" name="organization" required>
                      <input type="text" placeholder="Name" class="form-control2" name="name" required>
                      <input type="text" placeholder="Lastname" class="border-top-transparent form-control2" name="lastname" required>
                      <input type="email" placeholder="Email" class="border-top-transparent form-control2" name="email" required>
                      <input type="password" placeholder="Password" class="border-top-transparent form-control2" name="password" required>
                      <input type="password" placeholder="Confirm Password" class="border-top-transparent form-control2" name="password_confirmation" required>
                    </div>
                  </div>
                  <button type="submit" class="btn-danger btn-md btn-block">Send it</button>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
          <!-- //end row -->
        </div>
      </div>
    </div>
