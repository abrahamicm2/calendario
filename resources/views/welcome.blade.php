<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<!-- Styles -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.css' rel='stylesheet' />
<link href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.print.css' rel='stylesheet' media='print' />
<!--  js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.2/fullcalendar.min.js'></script>

<style>
html, body 
{
    font-family: 'Raleway', sans-serif;
    font-weight: 100;
     margin: 0;
     color: gray;

}
.banner1
{
    /*height:300px*/ ;
    width:100% ;
}
.home {
    font-size:25px;
    margin-right: 90px;
     font-weight: 600;
}
.a {
    font-size:20px;
    margin-right: 30px;
    /*color: #525252;
     color: #636b6f;*/
    /* padding: 0 25px;*/
     /*font-size: 12px;*/
     font-weight: 600;
     letter-spacing: .1rem;
     text-decoration: none;
     /*text-transform: uppercase;*/
}
.item1 {
    font-size:15px;
    margin-left: 0px;
}
.container-fluid{
    margin: 0;
    /*margin-left: 40px;
    margin-right: 40px;
    margin-top: 35px;*/
}
.login {
    position:absolute;
    right: 40px;
}
p {
    margin: 0;
    color: #898989;
     font-weight: 600;
}
.cuadrado {
    height: 200px;
    
    color: black;
}
html, body {
    background-color: #fff;
    color: #636b6f;
    font-family: 'Raleway', sans-serif;
    font-weight: 100;
    /*height: 100vh;*/
    margin: 0;
}
.full-height {
    /*height: 100vh;*/
}
.flex-center {
    align-items: center;
    display: flex;
    justify-content: center;
}
.position-ref {
    position: relative;
}
.top-right {
    position: absolute;
    right: 10px;
    top: 18px;
}
.content {
    text-align: center;
}
.title {
    font-size: 84px;
}
.links > a {
    color: #636b6f;
    padding: 0 25px;
    font-size: 12px;
    font-weight: 600;
    letter-spacing: .1rem;
    text-decoration: none;
    text-transform: uppercase;
}
.m-b-md {
    margin-bottom: 30px;
}
</style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <a href="" class="home">Home</a>
            <a href="" class="a">EVENT</a>
            <a href="" class="a">JOBS</a>
        </div>
        <div class="col-md-2">
            
              <a href="/server.php/home" class="a">LOGIN</a>
        </div>     
    </div>

    <div class="row">
        <div class="col-md-12">
            <img src="/storage/app/public/5.png" class="banner1">
        </div>
    </div>
    <br>

    <div class="row item1">
        <p class="">Calendar</p>
        <input type="date" name="">

    </div>
    <br><br>
    <div id='calendar' style="width:900px;margin: auto;"></div>
    <br>

    <div class="row">
        <div class="col-md-6">
            <div class="col-md-5">
               <img src="/storage/app/public/6.png" class="img-responsive"> 
            </div>
            <div class="col-md-7" >
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>           
        </div>
        <div class="col-md-6">
            <div class="col-md-5">
               <img src="/storage/app/public/6.png" class="img-responsive"> 
            </div>
            <div class="col-md-7">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
            
        </div>
    <a href="" class="a">View more</a>
    </div>
    <br>
<br>
<div class="row">
    <div class="col-md-12">
        <img src="/storage/app/public/7.png" style="width: 100%">
    </div>
</div>
</div>
</body>
<script>

    $(document).ready(function() {
        
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: new Date(),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: 
            [
      

                @foreach($parties as $party)
                @if($party->confirmado=="si")
                {
                    title: '{{$party->name}}',
                    start: '{{$party->day}}T{{$party->start}}',
                    end: '{{$party->day}}T{{$party->end}}'
                },

                @endif
                @endforeach

                

            ]
        });
        
    });

</script>

</html>


   <!--      <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
        </div> -->