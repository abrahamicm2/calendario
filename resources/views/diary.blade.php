@extends('layouts.app')

@section('content')
<!-- calendars -->
<style>

	
	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>



	<div id='calendar'></div>
<!-- end calendars -->

@endsection
@section('script')
<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listWeek'
			},
			defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
            events: 
            [
				// {
				// 	title: 'Long Event',
				// 	start: '2017-10-07',
				// 	end: '2017-10-10'
				// },
				// {
				// 	id: 999,
				// 	title: 'Repeating Event',
				// 	start: '2017-10-09T16:00:00'
				// },
				

				@foreach($parties as $party)
				@if($party->confirmado=="si")
				{
					title: '{{$party->name}}',
					start: '{{$party->day}}T{{$party->start}}',
					end: '{{$party->day}}T{{$party->end}}'
				},

				@endif
				@endforeach

				

			]
		});
		
	});

</script>

<!-- end calendar -->

@endsection
