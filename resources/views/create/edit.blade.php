@extends('layouts.app')

@section('content')

<div class="container">
<div class="col-md-8">
  <h2 class="text-center">GET CONNECTED CALENDAR EVENTS</h2>
  @if(count($errors) > 0)
  <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" 
data-toggle="modal" data-target="#myModal"
id="ocultar"
style="display:none"
>Open Modal

</button>
<script>
$(document).ready(function(){
  $("#ocultar").click()
   
});

</script>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Required</h4>
      </div>
      <div class="modal-body">
      @foreach($errors->all() as $error)
{{$error}} <br>
@endforeach

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endif


  {!! Form::open(array('url' => 'event/' . $parties->id,'method' => 'put','files' => true,'class'=>'form-horizontal')) !!}

  
   <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
    <label for="name">Name</label>
      <input type="text" class="form-control" 
      id="name" placeholder="Here is a some text input"
       name="name"
       value="{{ $parties->name }}"
       >
 
      <label for="description">Category</label>
      <input type="text" class="form-control" id="description"
       placeholder="Here is a some text input" name="description" value="{{ $parties->description }}">
    
  
    </div>
          <label >Times:</label><br>
      <input type="date" class="form-control" id="dstart" placeholder=" Date Start:" name="dstart" style="display: inline; width: 49%" value="$parties->dstart"> 
      <input type="time" class="form-control" id="start" placeholder="Start:" name="start" style="display: inline; width: 49%" value="$parties->start"> <br>
      <input type="date" class="form-control" id="dend" placeholder="Date:" name="dend" style="display: inline; width: 49%" valu="$parties->dend">
      <input type="time" class="form-control" id="end" placeholder="End:" name="end" style="display: inline; width: 49%" value="$parties->end">
<br>

      <div class="form-group">
      <label for="address">address:</label>
      <input type="text" class="form-control" id="address" placeholder="Here is a some text input"
       name="address" value="return $parties;">
    </div>

       
     
      <div class="form-group">

      <label for="image" >image: <img src="/storage/app/{{$parties->image}}" style="width: 300px"> </label>
      <input type="file" class="form-control" id="image" placeholder="Here is a some text input" 
      name="image" value="{{$parties->image}}" style="display: none">
    </div>
      

       <div class="form-group">
      <label for="pdf">pdf:</label>
      <input type="file" class="form-control" id="image" placeholder="Here is a some text input" 
      name="pdf" value="{{$parties->pdf}}">
      
    </div>
    
    
    
    <br>
    @if(Auth::user()->type=="admin")
    <label for="sel1">Confirmed:</label>
      <select class="form-control" id="sel1" name="confirmado" >
      @if($parties->confirmado=="si")
      <option value="si">Yes</option>
      <option value="no">No</option>
      @else
      <option value="no">No</option>
      <option value="si">Yes</option>

      
      @endif
    <input type="hidden" name="notified" value="si">

    @endif

  
      
      </select>
      <br>
   
    <button type="submit" class="btn btn-primary">Update</button>
    <a href="{{url('event')}}" class="btn btn-danger">Cancel</a>
    {!! Form::close() !!}
  </div>
</div>

@endsection