



<br>

<div class="row">

            
            <div class="col-md-12" style="padding: 60px">
              <div class="row">
                <div class="col-md-9">
                    <h3>EVENT MANAGEMENT</h3>
                    <a type="button" href="event/create" class="btn btn-success"> <span class="glyphicon glyphicon-plus "></span> New</a>                
                </div>

                <div class="col-md-3">
                  <br> <br> <br>
                    <div class="input-group" >
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-addon"> <span class="glyphicon glyphicon-search"></span>
                       
                    </div>
                </div>
                 

              </div>
               <br>
                  
                <div class="table-responsive">
                   <table class="table">
                      <thead>
                         <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Imagen</th>
                            <th>Day</th>
                            <th>Address</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Action</th>
                         </tr>
                      </thead>
                      <tbody>
                         <tr>

                         @foreach($parties as $party)

                         @if(Auth::user()->type=="member" && $party->confirmado=="no" && Auth::user()->name !=  $party->user)
                         @else
                        
                         


                         <td><a href="see/{{$party->id}}">{{ $party->name }}</a></td>
                            <td>{{ $party->description }}</td>
                            <td>aqui<img src="{{asset('storage/app/'.$party->image)}}" class="img-responsive img-rounded"  style="width:204px;height:auto;"></td>
                            <td>{{ $party->day}}</td>
                            <td>{{ $party->address}}</td>
                            <td>{{$party->start}} -  {{$party->end}} </td>
                            <td>@if($party->confirmado=="no") 
                                   

                            <p><span class="glyphicon glyphicon-remove" style="color:red"></span></p>    
                                @else
                            <span class="glyphicon glyphicon-ok " style="color:#5cb85c"></span>
                            
                            </td>
                            @endif
                            
                            <td> 
                            @if(Auth::user()->type=="admin")
                               <a href="event/{{$party->id}}/edit"class="btn btn-success btn-sm">
                               <span class="glyphicon glyphicon-edit"></span>
                               </a>
                               
                               <a href="event/{{$party->id}}" class="btn btn-danger btn-sm">
                               <span class="glyphicon glyphicon-trash"></span>
                               </a>
                               @endif
                               
                               @if($party->pdf!="Null")
                               <a class="btn btn-default btn-sm"
                                href="/storage/app/{{$party->pdf}}"
                                download="{{$party->description}}.pdf"
                                >
                               <span class="glyphicon glyphicon-download"></span>
                               </a>
                               
                               @else
                              <!--  <a class="btn btn-default btn-sm"
                                
                                 disabled="true"> 
                               <span class="glyphicon glyphicon-download"></span>
                               </a> -->
                               

                            </td>
                            @endif
                         </tr>
                         @endif
                         @endforeach
                         
                         
                      </tbody>
                   </table>
                </div>
              
             </div>
            
         
          </div>
          
<canvas id="the-canvas"></canvas>

@endsection

