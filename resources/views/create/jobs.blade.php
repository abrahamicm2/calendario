@extends('layouts.app')

@section('content')

<div class="container">
<div class="col-md-8">
  <h2 class="text-center">GET CONNECTED JOBS IN THE COMMUNITY</h2>
 
  @if(count($errors) > 0)
  <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" 
data-toggle="modal" data-target="#myModal"
id="ocultar"
style="display:none"
>Open Modal

</button>
<script>
$(document).ready(function(){
  $("#ocultar").click()
   
});

</script>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Required</h4>
      </div>
      <div class="modal-body">
      @foreach($errors->all() as $error)
{{$error}} <br>
@endforeach

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endif




  {!! Form::open(array('url' => 'event','method' => 'post','files' => true,'class'=>'form-horizontal')) !!}

  
   <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">

       
    <label for="jobs">Job title:</label>
      <input type="text" class="form-control" id="jobs" 
      placeholder="Here is a some text input" name="jobs">
      
     <p>Open from:</p>

      <label for="description">Descripcion</label>
      <input type="text" class="form-control" id="description" placeholder="Here is a some text input" name="description">

        <div class="form-group">
      <label >Times:</label>
      <input type="time" class="form-control" id="start" placeholder="Start:" name="start" style="display: inline; width: 40%" value="08:00">
      <input type="time" class="form-control" id="end" placeholder="End:" name="end" style="display: inline; width: 40%" value="12:00">

    </div>

    
      <label for="web">web:</label>
      <input type="date" class="form-control" id="web" placeholder="Here is a some text input" name="web" value="2017-11-11" >
    </div>
  
     <div class="form-group">
      <label for="address">address:</label>
      <input type="text" class="form-control" id="address" placeholder="Here is a some text input" name="address">
    </div>

       
      <label for="image">image:</label>
      <input type="file" class="form-control" id="image" placeholder="Here is a some text input" 
      name="image"  required="true"> 
    
      <label for="pdf" >pdf:</label>
      <input type="file" class="form-control" id="pdf" placeholder="Here is a some text input"
       name="pdf" >
    
    @if(Auth::user()->type=="admin")
   
    <label for="sel1">Confirmed:</label>
      <select class="form-control" id="sel1" name="confirmado">
      <option value="no">No</option>
    <option value="si">Yes</option>
      </select>
      <br>


    <br>
    <input type="hidden" name="notified" value="si" >
    @endif
    <button type="submit" class="btn btn-primary">Create</button>
    <a href="{{url('/event')}}" class="btn btn-danger">Cancel</a>
    {!! Form::close() !!}
  </div>
</div>


<!-- $errors->default->first("name") -->

@endsection