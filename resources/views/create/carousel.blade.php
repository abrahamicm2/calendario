 <section id="intro">
      <div id="carousel-example-generic" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner text-uppercase" role="listbox">
          <!-- First slide -->
          <div class="item carousel-item-one active">
            <div class="container">
              <h3 class="carousel-position-one animate-delay carousel-title-v1" data-animation="animated fadeInDown">
                GET CONNECTED
              </h3>
              <p class="carousel-position-two animate-delay carousel-subtitle-v1" data-animation="animated fadeInDown">
                Publish your Events or job opportunities <br> Free access for our partners
              </p>
              <a href="#" class="carousel-position-three animate-delay btn-brd-white" data-animation="animated fadeInUp">Learn More</a>
            </div>
          </div>
          <!-- Second slide -->
          <div class="item carousel-item-two">
            <div class="container">
              <h3 class="carousel-position-one animate-delay carousel-title-v2" data-animation="animated fadeInDown">
                Get to know the <br>events of the city
              </h3>
              <p class="carousel-position-three animate-delay carousel-subtitle-v2" data-animation="animated fadeInDown">
                Available in: Android &amp; IOS
              </p>
            </div>
          </div>
          <!-- Third slide -->
          <div class="item carousel-item-three">
            <div class="center-block">
              <div class="center-block-wrap">
                <div class="center-block-body">
                  <h3 class="margin-bottom-20 animate-delay carousel-title-v1" data-animation="animated fadeInDown">
                    Learn about
                  </h3>
                  <p class="margin-bottom-20 animate-delay carousel-title-v1" data-animation="animated fadeInDown">
                    job opportunities
                  </p>
                  <!--<a href="#" class="animate-delay btn-brd-white" data-animation="animated fadeInUp">Learn More</a> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>