Skip to content
This repository
Search
Pull requests
Issues
Marketplace
Explore
 @abrahamicm
 Sign out
 Watch 0
  Star 0  Fork 0 abrahamicm/documentacion
 Code  Issues 0  Pull requests 0  Projects 0  Wiki  Insights  Settings
Branch: master Find file Copy pathdocumentacion/bootstrap
4ec5da7  9 hours ago
@abrahamicm abrahamicm Create bootstrap
1 contributor
RawBlameHistory     
304 lines (274 sloc)  18.8 KB
Affix
.affix	The Affix plugin allows an element to become affixed (locked/sticky) to an area on the page. It toggles position:fixed on and off	

Alert
.alert	Creates an alert message box	
.alert-danger	Red alert box. Indicates a dangerous or potentially negative action	
.alert-dismissable	Together with the .close class, this class is used to close the alert	
.alert-info	Light-blue alert box. Indicates some information	
.alert-link	Used on links inside alerts to add matching colored links	
.alert-success	Green alert box. Indicates a successful or positive action	
.alert-warning	Yellow alert box. Indicates caution should be taken with this action	
.fade	Adds a fading effect when closing an alert box	

Badges
.badge	Creates a circular badge (grey circle - often used as a numerical indicator)	

Button Groups
.btn-group	Groups buttons together on a single line	Button 
.btn-group-justified	Makes a group of buttons span the entire width of the screen.
.btn-group-lg	Large button group (makes all buttons in a button group larger - increased font-size and padding).
.btn-group-sm	Small button group (makes all buttons in a button group smaller).
.btn-group-xs	Extra small button group (makes all buttons in a button <gr></gr>oup extra small).
.btn-group-vertical	Makes a button group appear vertically stacked.

Button
.active	Adds a blue background color to simulate a "pressed" button	
.btn	Creates a basic button (gray background and rounded corners)	
.btn-block	Creates a block level button that spans the entire width of the parent element	
.btn-danger	Red button. Indicates danger or a negative action	
.btn-default	Default button. White background and grey border	
.btn-info	Light-blue button. Represents information	
.btn-link	Makes a button look like a link (get button behavior)	
.btn-lg	Large button	
.btn-primary	Blue button.	
.btn-sm	Small button	
.btn-success	Green button. Indicates success or a positive action	
.btn-warning	Yellow button. Represents warning or a negative action	
.btn-xs	Extra small button	
.disabled	Disables a button (adds opacity and a "no-parking-sign" icon on hover)	

Corrousel
.carousel	Creates a carousel (slideshow)	
.carousel-caption	Creates a caption text for each slide in the carousel	
.carousel-control	Container for next and previous links	
.carousel-indicators	Adds little dots/indicators at the bottom of each slide (which indicates how many slides there is in the carousel, and which slide the user are currently 
.carousel-inner	Container for slide items	
.icon-next	Unicode icon (arrow pointing right), used in carousels. This is often replaced with a glyphicon	
.icon-prev	Unicode icon (arrow pointing left), used in carousels. This is often replaced with a glyphicon	
.item	Class added to each carousel item. May be text or images	
.left	Used to identify the left carousel control	
.next	Used in the carousel control to identity the next control	
.prev	Used in carousels to indicate a "previous" link	
.right	Used to identify the right carousel control	

Collapse
.collapse	Indicates collapsible content - which can be hidden or shown on demand	
.collapse in	Show the collapsible content by default	
.panel-collapse	Collapsible panel (toggle between hiding and showing panel(s))	

Containers
.container	Fixed width container with widths determined by screen sites. Equal margin on the left and right.	
.container-fluid	A container that spans the full width of the screen	

Dropdowns
.active	Adds a blue background color to the active dropdown item in a dropdown	
.caret	Creates a caret arrow icon , which indicates that the button is a dropdown	
.disabled	Disables a dropdown item (adds a grey text color and a "no-parking-sign" icon on hover)	
.divider	Used to separate links in the dropdown menu with a thin horizontal border	
.dropdown	Creates a toggleable menu that allows the user to choose one value from a predefined list	
.dropdown-header	Used to add headers inside the dropdown menu	
.dropdown-menu	Adds the default styles for the dropdown menu container	
.dropdown-menu-right	Right-aligns a dropdown menu	
.dropdown-toggle	Used on the button that should hide and show (toggle) the dropdown menu	
.dropup	Indicates a dropup menu (upwards instead of downwards)	

forms
.control-label	Allows a label to be used for form validation	
.form-control	Used on input, textarea, and select elements to span the entire width of the page and make them responsive	
.form-group	Container for form input and label	
.form-inline	Makes a <form> left-aligned with inline-block controls (This only applies to forms within viewports that are at least 768px wide)	
.form-horizontal	Aligns labels and groups of form controls in a horizontal layout	
.has-danger	Adds a red color to the label and a red border to the input, as well as an error icon inside the input (used together with .has-feedback)	
.has-feedback	Adds feedback icons for inputs (checkmark, warning and error signs)	
.has-success	Adds a green color to the label and a green border to the input, as well as a checkmark icon inside the input (used together with .has-feedback)	
.has-warning	Adds a yellow/orange color to the label and a yellow/orange border to the input, as well as a checkmark icon inside the input (used together with .has-feedback)	

Glyphicons
.glyphicon	Creates an icon. Bootstrap provides 260 free glyphicons from the Glyphicons Halflings 
Glyphicons 

Grid
.col-*-*	Responsive grid (span 1-12 column). Extra small devices Phones (< 768px), Small devices Tablets (≥768px), Medium devices Desktops (≥992px), Large devices Desktops (Column values can be 1-12.	
.col-*-offset-*	Move columns to the right. These classes increase the left margin of a column by * columns	
.col-*-pull-*	Changes the order of the grid columns	
.col-*-push-*	Changes the order of the grid columns	
.row	Container for responsive columns	

helpers
.bg-danger	Adds a red background color to an element. Represents danger or a negative action	
.bg-info	Adds a light-blue background color to an element. Represents some information	
.bg-primary	Adds a blue background color to an element. Represents something important	
.bg-success	Adds a green background color to an element. Indicates success or a positive action	
.bg-warning	Adds a yellow background color to an element. Represents a warning or a negative action	
.center-block	Centers any element (Sets an element to display:block with margin-right:auto and margin-left:auto)	
.clearfix	Clears floats	
.close	Indicates a close icon	
.hidden	Forces an element to be hidden (display:none)	
.hidden-*	Hides content depending on screen size	
.hide	Deprecated. Use .hidden instead	
.invisible	Makes an element invisible (visibility:hidden). Note: Even though the element is invisible, it will take up space on the page	
.pre-scrollable	Makes a <pre> element scrollable (max-height of 350px and provide a y-axis scrollbar)	
.pull-left	Float an element to the left	
.pull-right	Float an element to the right	
.show	Shows an element (display:block)	
.sr-only	Hides an element on all devices except for screen readers	
.sr-only-focusable	Hides an element on all devices except for screen readers	
.visible-*	Deprecated as of v3.2.0. Used to show and/or hide content by device. Note: Use .hidden-* instead	
.visible-print-block	Displays the element (display:block) in print (pre)view	
.visible-print-inline	Displays the element (display:inline) in print (pre)view	
.visible-print-inline-block	Displays the element (display:inline-block) in print (pre)view	
.hidden-print	Hides the element (display:none) in print (pre)view	

Images
.caption	Adds a caption text inside a .thumbnail	
.embed-responsive	Container for embedded content. Makes videos or slideshows scale properly on any device	
.embed-responsive-16by9	Container for embedded content. Creates an 16:9 aspect ratio embedded content	
.embed-responsive-4by3	Container for embedded content. Creates an 4:3 aspect ratio embedded content	
.embed-responsive-item	Used inside .embed-responsive. Scales the video nicely to the parent element	
.img-circle	Shapes an image to a circle (not supported in IE8 and earlier)	
.img-responsive	Makes an image responsive	
.img-rounded	Adds rounded corners to an image	
.img-thumbnail	Shapes an image to a thumbnail (borders)	
.thumbnail	Adds a border around an element (often images or videos) to make it look like a thumbnail	

input
.help-block	A block of help text that breaks onto a new line and may extend beyond one line.	Input 
.input-lg	Large input field	Input 
.input-sm	Small input field	Input 
.checkbox	Container for checkboxes	
.checkbox-inline	Makes multiple checkboxes appear on the same line	
.input-group	Container to enhance an input by adding an icon, text or a button in front or behind it as a "help text"	
.input-group-lg	Large input group	
.input-group-sm	Small input group	
.input-group-addon	Together with the .input-group class, this class makes it possible to add an icon or help text next to the input field	
.input-group-btn	Together with the .input-group class, this class attaches a button next to an input. Often used as a search bar	
.form-control-feedback	Form validation class	Inputs 
.form-control-static	Adds plain text next to a form label within a horizontal form	Inputs 

List Group
.list-group	Creates a bordered list group for <li> elements	List 
.list-group-item	Added to each <li> element in the list group	List 
.list-group-item-heading	Creates a list group heading (used on other elements besides <li>)	List 
.list-group-item-text	Used for item text inside the list group (used on other elements besides<li>)	List 
.list-group-item-danger	Red background color for a list item in a list group	List 
.list-group-item-info	Light-blue background color for a list item in a list group	List 
.list-group-item-success	Green background color for a list item in a list group	List 
.list-group-item-warning	Yellow background color for a list item in a list group	List 
.active	Adds a blue background color to the active list item in a list group	List 
.disabled	Disables a list item in a list group (cannot be clicked - adds a grey background color and a "no-parking-sign" icon on hover)	List 

Media Objects
.media	Aligns media objects (like images or videos - often used for comments in a blog post etc)	Media 
.media-body	Text that should appear next to a media object	Media 
.media-heading	Creates a heading inside the media object	Media 
.media-list	Nested media lists	Media 
.media-object	Indicates a media object (image or video)	Media 
jumbotron
.jumbotron	Creates a padded grey box with rounded corners that enlarges the font sizes of the text inside it. Creates a big box for calling extra attention to some special content information	

labels
.label	Adds a grey rounded box to an element. Provides additional information about something (e.g. "New")	
.label-danger	Red label	
.label-info	Light-blue label	
.label-success	Green label	
.label-warning	Yellow label	

modals
.modal	Identifies the content as a modal and brings focus to it	
.modal-body	Defines the style for the body of the modal. Add any HTML markup here (p, img, etc)	
.modal-content	Styles the modal (border, background-color, etc). Inside this, add the modal's header, body and footer, if needed	
.modal-dialog	Sets the proper width and margin of the modal	
.modal-footer	The footer of the modal (often contains an action button and a close button)	
.modal-header	The header of the modal (often contains a title and a close button)	
.modal-lg	Large modal (wider than default)	
.modal-open	Used on the <body> element to prevent page scrolling (overflow:hidden)	
.modal-sm	Small modal (less width)	
.modal-title	The title of the modal	

Navbar
.active	Adds a gray background color to the active link in a default navbar. Adds a black background and a white color to the current link inside an inverted navbar.	
.icon-bar	Used in the navbar to create a hamburger menu (three horizontal bars)	
.nav .navbar-nav	Used on a <ul> container that contains the list items with links inside a navigation bar	
.navbar	Creates a navigation bar	
.navbar-brand	Added to a link or a header element inside the navbar to represent a logo or a header	
.navbar-btn	Vertically aligns a button inside a navbar	
.navbar-collapse	Collapses the navbar (hidden and replaced with a menu/hamburger icon on mobile phones and small tablets)	
.navbar-default	Creates a default navigation bar (light-grey background color)	
.navbar-fixed-bottom	Makes the navbar stay at the bottom of the screen (sticky/fixed)	
.navbar-fixed-top	Makes the navbar stay at the top of the screen (sticky/fixed)	
.navbar-form	Added to form elements inside the navbar to vertically center them (proper padding)	
.navbar-header	Added to a container element that contains the link/element that represent a logo or a header	
.navbar-inverse	Creates a black navigation bar (instead of light-grey)	
.navbar-left	Aligns nav links, forms, buttons, or text, in the navbar to the left	
.navbar-link	Styles an element to look like a link inside the navbar (anchors get proper padding and an underline on hover, while other elements like p or span gets a default hover - white color in an inversed navbar and a black color in a default navbar)	
.navbar-nav	Used on a <ul> container that contains the list items with links inside a navigation bar	
.navbar-right	Aligns nav links, forms, buttons, or text in the navbar to the right.	
.navbar-static-top	Removes left, top and right borders (rounded corners) from the navbar (default navbar has a gray border and a 4px border-radius by default)	
.navbar-text	Vertical align any elements inside the navbar that are not links (ensures proper padding)	
.navbar-toggle	Styles the button that should open the navbar on small screens. Often used together with three .icon-bar classes to indicate a toggleable menu icon (hamburger/bars)	

Page Header
.page-header	Adds a horizontal line under the heading (+ adds some extra space around the element)	Page 
.next	Used to align pager buttons to the right side of the page (next button)	
.pager	Creates previous/next buttons (used on <ul> elements)	
.previous	Used to align pager buttons to the left side of the page (previous button)	

pagination
.active	Adds a blue background color to the active pagination link (to highlight the current page)	
.breadcrumb	A pagination. Indicates the current page's location within a navigational hierarchy	
.disabled	Disables a pagination link (cannot be clicked - adds a grey text color and a "no-parking-sign" icon on hover)	
.pagination	Creates a pagination (Useful when you have a web site with lots of pages. Used on <ul> elements)	
.pagination-lg	Large pagination (each pagination link gets a font-size of 18px. Default is 14px)	
.pagination-sm	Small pagination (each pagination link gets a font-size of 12px. Default is 14px)	

Panels
.panel	Creates a bordered box with some padding around its content	
.panel-body	Container for content inside the panel	
.panel-danger	Red panel. Indicates danger	
.panel-info	Light-blue panel. Indicates information	
.panel-success	Green panel. Indicates success	
.panel-warning	Yellow panel. Indicates warning	
.panel-footer	Creates a panel footer (light background color)	
.panel-group	Used to group many panels together. This removes the bottom margin below each panel	
.panel-heading	Creates a panel header (light background color)	
.panel-title	Used inside a .panel-heading to adjust the styling of the text (removes margins and adds a font-size of 16px)	
popover
.popover	Popup-box that appears when the user clicks on an element	

progress bars
.active	Animates a striped progress bar	Progress 
.progress	Container for progress bars	Progress 
.progress-bar	Creates a progress bar	Progress 
.progress-bar-danger	Red progress bar. Indicates danger	Progress 
.progress-bar-info	Light-blue progress bar. Indicates information	Progress 
.progress-bar-striped	Creates a striped progress bar	Progress 
.progress-bar-success	Green progress bar. Indicates success	Progress 
.progress-bar-warning	Yellow progress bar. Indicates warning	Progress 

tables
.active	Adds a grey background color to the table row (<tr> or table cell (<td>) (same color used on hover)	
.danger	Adds a red background to the table row (<tr> or table cell (<td>). Indicates a dangerous or potentially negative action	
.info	Adds a light-blue background to the table row (<tr> or table cell (<td>). Indicates a neutral informative change or action	
.success	Adds a green background color to a table row (<tr> or table cell (<td>). Indicates success or a positive action	
.table	Adds basic styling to a table (padding, bottom borders, etc)	
.table-bordered	Adds borders on all sides of the table and cells	
.table-condensed	Makes a table more compact by cutting cell padding in half	
.table-hover	Creates a hoverable table (adds a grey background color on table rows on hover)	
.table-responsive	Makes a table responsive (adds a horizontal scrollbar when needed)	
.warning	Adds a yellow background color to the table row (<tr> or table cell (<td>). Indicates a warning	

Tabs
.in	Fades in tabs	
.list-inline	Places all list items on a single line (horizontal menu)	
.nav nav-tabs	Indicates a tabbed menu	
.nav nav-pills	Indicates a pill menu	
.nav-justified	Centers tabs/pills. Note that on screens smaller than 768px the items are stacked (content will remain centered)	
.nav-stacked	Vertically stack tabs or pills	
.nav-tabs	Creates a tabbed menu	
.tab-content	Used together with .tab-pane to creates toggleable/dynamic tabs/pills	
.tab-pane	Used together with .tab-content to creates toggleable/dynamic tabs/pills	

Tolptips
.tooltip	Popup-box that appears when the user moves the mouse pointer over an element	

Typography
.dl-horizontal	Lines up the terms <dt> and descriptions <dd> in <dl> elements side-by-side. Starts off like default <dl>s, but when the browser window expands, it will line up side-by-	
.h1 - .h6	Makes an element look like a heading of the chosen class (h1-h6)	
.initialism	Displays the text inside an <abbr> element in a slightly smaller font size	
.lead	Increase the font size and line height of a paragraph	
.list-unstyled	Removes all default list-style (bullets, left margin, etc.) styling from a <ul> or <ol> list	
.mark	Highlights text: Highlighted text	
.small	Creates a lighter, secondary text in any heading	
.text-capitalize	Indicates capitalized text	
.text-center	Center-aligns text	
.text-danger	Red text color. Indicates danger	
.text-hide	Hides text (helps replace an element's text content with a background image)	
.text-info	Light-blue text color. Indicates information	
.text-justify	Indicates justified text	
.text-left	Aligns the text to the left	
.text-lowercase	Changes text to lowercase	
.text-muted	Grey text color	
.text-nowrap	Prevents the text from wrapping	
.text-primary	Blue text color	
.text-right	Aligns text to the right	
.text-success	Green text color. Indicates success	
.text-uppercase	Makes text uppercase	
.text-warning	Yellow/orange text color. Indicates warning	

well
.well	Adds a rounded border around an element with a gray background color and some padding	
.well-lg	Large well (more padding)	
.well-sm	Small well (less padding)	

© 2017 GitHub, Inc.
Terms
Privacy
Security
Status
Help
Contact GitHub
API
Training
Shop
Blog
About